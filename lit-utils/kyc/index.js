const kalapa = require('./providers/kalapa')
const KYC_PROVIDER = {
  KALAPA: 'kalapa'
}
class KycFacade {
  static provider(provider) {
    switch (provider) {
      case KYC_PROVIDER.KALAPA:
        return kalapa
      default:
        return kalapa
    }
  }

  static async getUserInfoByIDImage(idPhoto, selfie) {
    return await KycFacade.provider().getUserInfoByIDImage(idPhoto, selfie)
  }
  static async antifraud(id) {
    return await KycFacade.provider().antifraud(id)
  }

  static async userScore(id) {
    return await KycFacade.provider().userScore(id)
  }

  static async creditInfo(id) {
    return await KycFacade.provider().creditInfo(id)
  }

  static mappingIdInfo(res) {
    return KycFacade.provider().mappingIdInfo(res)
  }
  static mappingAntifraud(res) {
    return KycFacade.provider().mappingAntifraud(res)
  }
  static mappingScore(res) {
    return KycFacade.provider().mappingScore(res)
  }
  static mappingCredit(res) {
    return KycFacade.provider().mappingCredit(res)
  }
}

module.exports = {
  KycFacade,
  KYC_PROVIDER
}
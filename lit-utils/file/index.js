const s3 = require('./providers/s3')
const FILE_PROVIDER = {
  S3: 's3'
}

class FileFacade {
  static provider(provider) {
    switch (provider) {
      case FILE_PROVIDER.S3:
        return s3
      default:
        return s3
    }
  }

  static put(filePath, content, options) {
    return FileFacade.provider().put(filePath, content, options)
  }

  static uploadKycResponse(filePath, content) {
    return FileFacade.provider().uploadKycResponse(filePath, content)
  }

  static zip(folders, res, bucket) {
    return FileFacade.provider().zip(folders, res, bucket)
  }
}

module.exports = {
  FileFacade,
  FILE_PROVIDER
}
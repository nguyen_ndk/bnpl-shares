const bcrypt = require('bcryptjs');
const _ = require('lodash');
const Validator = require('jsonschema').Validator;
const { INSTALLMENT, DEFAULT_CURRENCY, DEFAULT_LOCALE, PROMOTION } = require('lit-constants');
const { v4: uuidv4 } = require('uuid');
const rTracer = require('cls-rtracer');

const currencyFormator = new Intl.NumberFormat(DEFAULT_LOCALE, { style: 'currency', currency: DEFAULT_CURRENCY });
const calcInstallment = (amount, total = INSTALLMENT.NUMBER) => {
  return {
    amount: _.ceil(amount / total),
    total: total,
  };
};

const calcInstallmentPaidDate = (beginDate, amount, total = INSTALLMENT.NUMBER, dueGap = INSTALLMENT.DUE_GAP) => {
  const result = [{ dueDate: beginDate.toISOString(), amount, installmentNumber: 1 }];
  for (let i = 1; i < total; i++) {
    result.push({ dueDate: beginDate.add(dueGap, 'd').toISOString(), amount, installmentNumber: i + 1 });
  }
  return result;
};

const calcPurchaseRemaining = (totalInstallment, paidInstallment, totalAmount) => {
  const each = _.round(totalAmount / totalInstallment);
  return { remaining: (totalInstallment - paidInstallment) * each, each };
};

const calcPurchaseNotDueAmount = (totalInstallment, dueInstallments, totalAmount) => {
  const each = _.round(totalAmount / totalInstallment);
  return (totalInstallment - dueInstallments) * each;
};
// TODO replace with calcUserCreditAfterNewPurchase
const userCreditAfterFirstPurchase = (current, purchaseAmount, installmentAmount) => {
  return current - purchaseAmount + installmentAmount;
};
// TODO replace with calcUserCreditAfterNewPurchase
const calcCreditHistoryFistPurchase = (total, installmentAmount) => {
  return total - installmentAmount;
};

const calcUserCreditAfterNewPurchase = (current, purchaseAmount, installmentAmount) => {
  const changed = purchaseAmount - installmentAmount;
  return {
    changed,
    newCredit: current - changed,
  };
};

const generatePassword = (password) => {
  return bcrypt.hashSync(password, 10);
};

const genMerReqId = (data) => `${data.clientId}-${data.requestId}`;
const random6 = () => Math.floor(Math.random() * 899999 + 100000);

const randomNumber = (lenght = 4) => {
  var digits = '0123456789';
  let OTP = '';
  for (let i = 0; i < lenght; i++) {
    OTP += digits[Math.floor(Math.random() * 10)];
  }
  return OTP;
};

const currencyFormat = (number) => {
  return currencyFormator.format(number);
};

const addQueriesToUrl = (url, queries) => {
  let urlObj = new URL(url);
  _.forEach(queries, (v, k) => {
    urlObj.searchParams.append(k, v);
  });
  return urlObj.toString();
};

const addFailedAmount = (installments, remaining) => {
  _.forEach(installments, (i) => {
    remaining += i.amount;
  });
  return remaining;
};
const rewriteJson = (instance, schema) => {
  if (schema.type === 'string' && schema.max) {
    return _.truncate(instance, { length: schema.max });
  }
  return instance;
};
const truncateJson = (json, schema) => {
  const v = new Validator();
  const res = v.validate(json, schema, { rewrite: rewriteJson });
  return res.instance;
};

const isEmptyReturnNull = (value) => {
  var result = value;
  if (_.isEmpty(value)) result = null;

  return result;
};

const promotionMaxVoucerClaim = (bugetMax, type, amount, maxDiscount) => {
  switch (type) {
    case PROMOTION.TYPE.PERCENTAGE:
      return parseInt(bugetMax / maxDiscount);
    case PROMOTION.TYPE.AMOUNT:
      return parseInt(bugetMax / amount);
  }
  return 0;
};

const getReqId = () => rTracer.id();

//TODO : Logic generate Client ID for Merchant
const generateClientId = () => uuidv4();

module.exports = {
  randomNumber,
  generateClientId,
  getReqId,
  generatePassword,
  calcInstallment,
  calcInstallmentPaidDate,
  genMerReqId,
  calcPurchaseRemaining,
  userCreditAfterFirstPurchase,
  calcCreditHistoryFistPurchase,
  random6,
  currencyFormat,
  addQueriesToUrl,
  addFailedAmount,
  truncateJson,
  calcPurchaseNotDueAmount,
  isEmptyReturnNull,
  promotionMaxVoucerClaim,
  calcUserCreditAfterNewPurchase,
};

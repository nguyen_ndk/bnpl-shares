const xero = require('./providers/xero')

const ACCOUNTING_PROVIDER = {
  XERO: 'xero'
}

class Accounting {
  static provider(provider) {
    switch (provider) {
      case ACCOUNTING_PROVIDER.XERO:
        return xero
      default:
        return xero
    }
  }

  static getLoginUrl() {
    return Accounting.provider().getLoginUrl()
  }

  static syncUsers(users, auth) {
    return Accounting.provider().syncUsers(users, auth)
  }

  static syncPurchase(purchase, installments, auth) {
    return Accounting.provider().syncPurchase(purchase, installments, auth)
  }

  static syncPaymentGatewayFee(desc, amount, user, auth) {
    return Accounting.provider().syncPaymentGatewayFee(desc, amount, user, auth)
  }

  static refreshToken(auth) {
    return Accounting.provider().refreshToken(auth)
  }

  static syncMerchants(merchants, auth) {
    return Accounting.provider().syncMerchants(merchants, auth)
  }

  static decode(url) {
    return Accounting.provider().decode(url)
  }

  static buildUserData(user) {
    return Accounting.provider().buildUserData(user)
  }

  static buildMerchantData(user) {
    return Accounting.provider().buildMerchantData(user)
  }
  static getAuthKey() {
    return Accounting.provider().authKey
  }
}

module.exports = {
  ACCOUNTING_PROVIDER,
  Accounting
}

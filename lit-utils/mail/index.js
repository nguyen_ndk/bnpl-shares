const NodeMailer = require('./providers/nodemailer')

const getMailerProvider = (name) => {
  switch (name) {
    case 'nodemailer': 
      return new NodeMailer()
    default:
      return new NodeMailer()
  }
}

module.exports = {
  EmailFacade: getMailerProvider()
}
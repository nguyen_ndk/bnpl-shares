const {createLogger, transports, format} = require('winston')
const helpers = require('../../helpers')
const { printf } = format
const formatter = printf(info => {
  const rid = helpers.getReqId()
  let text = `${info.timestamp} [request-id:${rid}] [${info.level.toUpperCase()}]: ${info.message}`
  const splat = info[Symbol.for('splat')]
  if (splat !== undefined && splat.length !== 0) {
    text += ' CONTEXT: '
    splat.forEach(item => {
      if (item instanceof Error) {
        //TODO remove console.log
        console.log(item)
        text += item.toString()
        return
      }
      text += JSON.stringify(item, null, 2)
    })
  }
  return text
})

const logger = createLogger({level: 'error',
  transports: [
    new transports.Console({
      format: format.combine(
        format.timestamp({
          format: 'YYYY-MM-DD HH:mm:ss'
        }),
        formatter
      ),
      level: 'info',
    })
  ],
})

if (process.env.NODE_ENV !== 'production') {
  logger.add(new transports.File({
    filename: 'error.log',
    level: 'error',
    format: format.combine(
      format.timestamp({
        format: 'YYYY-MM-DD HH:mm:ss'
      }),
      formatter
    ),
  }))
  logger.add(new transports.File({
    filename: 'combined.log',
    level: 'info',
    format: format.combine(
      format.timestamp({
        format: 'YYYY-MM-DD HH:mm:ss'
      }),
      formatter
    ),
  }))
}

module.exports = logger
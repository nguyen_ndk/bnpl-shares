const sqs = require('./providers/sqs')
const QUEUE_PROVIDER = {
  SQS: 'sqs'
}
class QueueFacade {
  static provider(provider) {
    switch (provider) {
      case QUEUE_PROVIDER.SQS:
        return sqs
      default:
        return sqs
    }
  }

  static async send(body, options) {
    return await QueueFacade.provider().send(body, options)
  }

  static async sendBatch(messages, options) {
    return await QueueFacade.provider().sendBatch(messages, options)
  }

  static async createConsumer(queue, handler, errorHandler, processingErrorHandler) {
    return await QueueFacade.provider().createConsumer(queue, handler, errorHandler, processingErrorHandler)
  }
}

module.exports = {
  QueueFacade,
  QUEUE_PROVIDER
}
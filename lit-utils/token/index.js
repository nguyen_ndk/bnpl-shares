const jwt = require('jsonwebtoken')
const TOKEN_PROVIDER = {
  JWT: 'jwt'
}

class TokenFacade {
  static provider(provider) {
    switch (provider) {
      case TOKEN_PROVIDER.JWT:
        return jwt
      default:
        return jwt
    }
  }

  static sign(body, secret, expiration) {
    const opts = {}
    if (expiration) {
      opts.expiresIn = expiration
    }
    return TokenFacade.provider().sign(body, secret, opts)
  }
  static verify(token, secret) {
    return TokenFacade.provider().verify(token, secret)
  }
}

module.exports = {
  TokenFacade,
  TOKEN_PROVIDER
}
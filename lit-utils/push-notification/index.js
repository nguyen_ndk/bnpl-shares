const {isFunction} = require('lodash')
const onesignal = require('./providers/onesignal')
const PUSH_NOTIFICATION_PROVIDER = {
  ONESIGNAL: 'onesignal',
}
class PushNotification {
  static provider(provider) {
    switch (provider) {
      case PUSH_NOTIFICATION_PROVIDER.ONESIGNAL:
        return onesignal
      default:
        return onesignal
    }
  }

  static sendPos(headings, contents, data, pushNotificationIds) {
    const provider = PushNotification.provider()
    if (!isFunction(provider.sendPos)) {
      return Promise.reject(new Error('NON_SUPPORT_FUNCTION sendPos'))
    }
    return PushNotification.provider().sendPos(headings, contents, data, pushNotificationIds)
  }

  static sendUser(headings, contents, data, pushNotificationIds) {
    const provider = PushNotification.provider()
    if (!isFunction(provider.sendUser)) {
      return Promise.reject(new Error('NON_SUPPORT_FUNCTION sendUser'))
    }
    return PushNotification.provider().sendUser(headings, contents, data, pushNotificationIds)
  }
}

module.exports = {
  PushNotification,
  PUSH_NOTIFICATION_PROVIDER
}
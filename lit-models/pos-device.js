const { DataTypes, Model } = require('sequelize');
const {
  facade: { DBFacade },
  providers: { DB_PROVIDER },
} = require('lit-utils');
const { PosNotificationChannel } = require('./pos-notification-channel');
class PosDevice extends Model {}

PosDevice.init(
  {
    id: {
      type: DataTypes.INTEGER,
      autoIncrement: true,
      primaryKey: true,
      unique: true,
      allowNull: false,
    },
    pushNotificationId: {
      type: DataTypes.STRING(255),
      field: 'push_notification_id',
    },
    description: {
      type: DataTypes.STRING(255),
      field: 'description',
    },
    status: {
      type: DataTypes.INTEGER,
      allowNull: false,
    },
    merchantId: {
      type: DataTypes.INTEGER,
      allowNull: false,
      field: 'merchant_id',
    },
    createdAt: {
      field: 'created_at',
      type: DataTypes.DATE,
      allowNull: false,
    },
    updatedAt: {
      field: 'updated_at',
      type: DataTypes.DATE,
    },
    deletedAt: {
      type: DataTypes.DATE,
      field: 'deleted_at',
    },
  },
  {
    tableName: 'pos_devices',
    sequelize: DBFacade.provider(DB_PROVIDER.SEQUELIZE).getConnection(),
    modelName: 'PosDevice',
    paranoid: true,
    freezeTableName: true,
  },
);
PosDevice.hasMany(PosNotificationChannel, { foreignKey: 'posId' });
PosNotificationChannel.belongsTo(PosDevice, { foreignKey: 'posId' });
module.exports = {
  PosDevice,
};

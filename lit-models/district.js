const { DataTypes, Model } = require('sequelize');
const {
  facade: { DBFacade },
  providers: { DB_PROVIDER },
} = require('lit-utils');
const { Ward } = require('./ward');

class District extends Model {}

District.init(
  {
    id: {
      type: DataTypes.INTEGER,
      autoIncrement: true,
      primaryKey: true,
      unique: true,
      allowNull: false,
    },
    name: {
      type: DataTypes.STRING(50),
      allowNull: false,
    },
    cityId: {
      type: DataTypes.INTEGER,
      field: 'city_id',
      allowNull: false,
    },
    createdAt: {
      field: 'created_at',
      type: DataTypes.DATE,
      allowNull: false,
    },
  },
  {
    tableName: 'districts',
    sequelize: DBFacade.provider(DB_PROVIDER.SEQUELIZE).getConnection(),
    modelName: 'District',
    updatedAt: false,
  },
);

District.hasMany(Ward, { foreignKey: 'districtId' });
module.exports = {
  District,
};

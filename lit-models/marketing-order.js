const { DataTypes, Model } = require('sequelize');
const {
  facade: { DBFacade },
  providers: { DB_PROVIDER },
} = require('lit-utils');
// const {MarketingCategory} = require('./marketing-category')
// const  {Marketing} = require('./marketing')
class MarketingOrder extends Model {}

MarketingOrder.init(
  {
    marketingId: {
      field: 'marketing_id',
      type: DataTypes.INTEGER,
      allowNull: false,
    },
    categoriesId: {
      type: DataTypes.INTEGER,
      field: 'categories_id',
    },

    order: {
      type: DataTypes.INTEGER,
    },
    createdAt: {
      field: 'created_at',
      type: DataTypes.DATE,
    },

    updatedAt: {
      field: 'updated_at',
      type: DataTypes.DATE,
    },
    id: {
      type: DataTypes.INTEGER,
      autoIncrement: true,
      primaryKey: true,
      unique: true,
      allowNull: false,
    },
  },
  {
    tableName: 'marketingorder',
    sequelize: DBFacade.provider(DB_PROVIDER.SEQUELIZE).getConnection(),
    modelName: 'MarketingOrder',
  },
);

// MarketingOrder.hasMany(MarketingCategory, {foreignKey: 'categoriesId'} )
// MarketingCategory.belongsTo(MarketingOrder,{foreignKey: 'categoriesId'})
// MarketingOrder.hasMany(Marketing,{foreignKey: 'marketingId'})
// Marketing.belongsTo(MarketingOrder, {foreignKey: 'marketingId'} )

module.exports = {
  MarketingOrder,
};

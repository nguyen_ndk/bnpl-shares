const { DataTypes, Model } = require('sequelize');
const {
  facade: { DBFacade },
  providers: { DB_PROVIDER },
} = require('lit-utils');
class UserHasPromotion extends Model {}

UserHasPromotion.init(
  {
    id: {
      autoIncrement: true,
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true,
    },
    userId: {
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true,
      references: {
        model: 'users',
        key: 'id',
      },
      field: 'user_id',
    },
    promotionId: {
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true,
      references: {
        model: 'promotions',
        key: 'id',
      },
      field: 'promotion_id',
    },
    amountVoucher: {
      type: DataTypes.INTEGER,
      allowNull: false,
      field: 'amount_voucher',
    },
    discountAmount: {
      type: DataTypes.INTEGER,
      allowNull: true,
      field: 'discount_amount',
    },
    amountVoucherUsed: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0,
      field: 'amount_voucher_used',
    },
    status: {
      type: DataTypes.BOOLEAN,
      allowNull: false,
      defaultValue: false,
    },
    createdAt: {
      field: 'created_at',
      type: DataTypes.DATE,
    },
    updatedAt: {
      field: 'updated_at',
      type: DataTypes.DATE,
    },
    deletedAt: {
      field: 'deleted_at',
      type: DataTypes.DATE,
    },
  },
  {
    tableName: 'user_has_promotion',
    sequelize: DBFacade.provider(DB_PROVIDER.SEQUELIZE).getConnection(),
    modelName: 'UserHasPromotion',
  },
);

module.exports = {
  UserHasPromotion,
};

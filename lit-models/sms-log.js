const { DataTypes, Model } = require('sequelize');
const {
  facade: { DBFacade },
  providers: { DB_PROVIDER },
} = require('lit-utils');

class SmsLog extends Model {}

SmsLog.init(
  {
    type: {
      type: DataTypes.INTEGER,
    },
    isSuccess: {
      field: 'is_success',
      type: DataTypes.BOOLEAN,
    },
    phoneNumber: {
      field: 'phone_number',
      type: DataTypes.STRING,
    },
    message: {
      type: DataTypes.STRING,
    },
    carrier: {
      type: DataTypes.STRING,
    },
    provider: {
      type: DataTypes.STRING,
    },
    response: {
      type: DataTypes.STRING,
    },
    userId: {
      field: 'user_id',
      type: DataTypes.INTEGER,
    },
    createdAt: {
      field: 'created_at',
      type: DataTypes.DATE,
    },
    updatedAt: {
      field: 'updated_at',
      type: DataTypes.DATE,
    },
    deletedAt: {
      field: 'deleted_at',
      type: DataTypes.DATE,
    },
    id: {
      type: DataTypes.INTEGER,
      autoIncrement: true,
      primaryKey: true,
      unique: true,
      allowNull: false,
    },
  },
  {
    tableName: 'sms_logs',
    sequelize: DBFacade.provider(DB_PROVIDER.SEQUELIZE).getConnection(),
    modelName: 'SmsLog',
  },
);

module.exports = {
  SmsLog,
};

const { DataTypes, Model } = require('sequelize');
const {
  facade: { DBFacade },
  providers: { DB_PROVIDER },
} = require('lit-utils');
class PurchaseAccounting extends Model {}

PurchaseAccounting.init(
  {
    id: {
      type: DataTypes.INTEGER,
      autoIncrement: true,
      primaryKey: true,
      unique: true,
      allowNull: false,
    },
    purchaseId: {
      type: DataTypes.INTEGER,
      allowNull: false,
      field: 'purchase_id',
    },
    installmentId: {
      type: DataTypes.INTEGER,
      field: 'installment_id',
    },
    label: {
      type: DataTypes.STRING(255),
      allowNull: false,
    },
    financeId: {
      type: DataTypes.STRING(255),
    },
    status: {
      type: DataTypes.STRING(50),
    },
    amount: {
      type: DataTypes.FLOAT,
    },
    updatedAt: {
      type: DataTypes.DATE,
      field: 'updated_at',
    },
    createdAt: {
      type: DataTypes.DATE,
      allowNull: false,
      field: 'created_at',
    },
  },
  {
    tableName: 'purchases_accounting',
    sequelize: DBFacade.provider(DB_PROVIDER.SEQUELIZE).getConnection(),
    modelName: 'PurchaseAccounting',
    updatedAt: false,
  },
);
module.exports = { PurchaseAccounting };

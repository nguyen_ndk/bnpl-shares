const { DataTypes, Model } = require('sequelize');
const {
  facade: { DBFacade },
  providers: { DB_PROVIDER },
} = require('lit-utils');
class AppsFlyer extends Model {}

AppsFlyer.init(
  {
    id: {
      autoIncrement: true,
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true,
    },
    key: {
      type: DataTypes.STRING(255),
      allowNull: false,
    },
    eventName: {
      type: DataTypes.STRING(255),
      allowNull: false,
      field: 'event_name',
    },
    createdAt: {
      field: 'created_at',
      type: DataTypes.DATE,
    },
    updatedAt: {
      field: 'updated_at',
      type: DataTypes.DATE,
    },
  },
  {
    tableName: 'appsflyer',
    sequelize: DBFacade.provider(DB_PROVIDER.SEQUELIZE).getConnection(),
    modelName: 'AppsFlyer',
  },
);
module.exports = { AppsFlyer };

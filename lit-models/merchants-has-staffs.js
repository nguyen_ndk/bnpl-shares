const { DataTypes, Model } = require('sequelize');
const {
  facade: { DBFacade },
  providers: { DB_PROVIDER },
} = require('lit-utils');

class MerchantsHasStaffs extends Model {}

MerchantsHasStaffs.init(
  {
    id: {
      type: DataTypes.INTEGER,
      autoIncrement: true,
      primaryKey: true,
      unique: true,
      allowNull: false,
    },
    MerchantId: {
      type: DataTypes.INTEGER,
      field: 'merchant_id',
    },
    StaffId: {
      type: DataTypes.INTEGER,
      field: 'staff_id',
    },
  },
  {
    tableName: 'merchants_has_staffs',
    sequelize: DBFacade.provider(DB_PROVIDER.SEQUELIZE).getConnection(),
    modelName: 'MerchantsHasStaffs',
    timestamps: false,
  },
);

module.exports = {
  MerchantsHasStaffs,
};

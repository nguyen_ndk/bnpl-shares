const { DataTypes, Model } = require('sequelize');
const {
  facade: { DBFacade },
  providers: { DB_PROVIDER },
} = require('lit-utils');
const { Marketing } = require('./marketing');
const { MarketingHasCategory } = require('./marketing-has-category');
class MarketingCategory extends Model {}

MarketingCategory.init(
  {
    name: {
      type: DataTypes.STRING(255),
      allowNull: false,
    },
    updatedAt: {
      type: DataTypes.DATE,
      field: 'updated_at',
    },
    createdAt: {
      type: DataTypes.DATE,
      allowNull: false,
      field: 'created_at',
    },
    slug: {
      type: DataTypes.STRING(255),
    },
    listMarketing: {
      type: DataTypes.JSON,
      allowNull: true,
      field: 'list_marketing',
    },
    id: {
      type: DataTypes.INTEGER,
      autoIncrement: true,
      primaryKey: true,
      unique: true,
      allowNull: false,
    },
  },
  {
    tableName: 'marketing_categories',
    sequelize: DBFacade.provider(DB_PROVIDER.SEQUELIZE).getConnection(),
    modelName: 'MarketingCategory',
    updatedAt: false,
  },
);

MarketingCategory.belongsToMany(Marketing, { through: MarketingHasCategory, foreignKey: 'categoriesId' });
Marketing.belongsToMany(MarketingCategory, { through: MarketingHasCategory, foreignKey: 'marketingId' });
MarketingCategory.hasOne(MarketingHasCategory, { foreignKey: 'categoriesId' });
module.exports = { MarketingCategory };

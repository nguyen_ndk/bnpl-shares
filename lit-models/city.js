const { DataTypes, Model } = require('sequelize');
const {
  facade: { DBFacade },
  providers: { DB_PROVIDER },
} = require('lit-utils');
const { District } = require('./district');

class City extends Model {}

City.init(
  {
    id: {
      type: DataTypes.INTEGER,
      autoIncrement: true,
      primaryKey: true,
      unique: true,
      allowNull: false,
    },
    name: {
      type: DataTypes.STRING(50),
      allowNull: false,
    },
    countryId: {
      type: DataTypes.INTEGER,
      allowNull: false,
      field: 'country_id',
    },
    createdAt: {
      field: 'created_at',
      type: DataTypes.DATE,
      allowNull: false,
    },
  },
  {
    tableName: 'cities',
    sequelize: DBFacade.provider(DB_PROVIDER.SEQUELIZE).getConnection(),
    modelName: 'City',
    updatedAt: false,
  },
);

City.hasMany(District, { foreignKey: 'cityId' });

module.exports = {
  City,
};

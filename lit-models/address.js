const { DataTypes, Model } = require('sequelize');
const {
  facade: { DBFacade },
  providers: { DB_PROVIDER },
} = require('lit-utils');
const { City } = require('./city');
const { District } = require('./district');
const { Ward } = require('./ward');
class Address extends Model {}

Address.init(
  {
    id: {
      type: DataTypes.INTEGER,
      autoIncrement: true,
      primaryKey: true,
      unique: true,
      allowNull: false,
    },
    // Model attributes are defined here
    line1: {
      type: DataTypes.STRING(255),
      field: 'line_1',
      allowNull: false,
    },
    note: {
      type: DataTypes.STRING(255),
    },
    districtId: {
      type: DataTypes.INTEGER,
      allowNull: false,
      field: 'district_id',
    },
    cityId: {
      type: DataTypes.INTEGER,
      allowNull: false,
      field: 'city_id',
    },
    wardId: {
      type: DataTypes.INTEGER,
      allowNull: false,
      field: 'ward_id',
    },
    fullAddress: {
      type: DataTypes.STRING(255),
      field: 'full_address',
    },
    createdAt: {
      type: DataTypes.DATE,
      field: 'created_at',
      allowNull: false,
    },
  },
  {
    tableName: 'address',
    sequelize: DBFacade.provider(DB_PROVIDER.SEQUELIZE).getConnection(),
    modelName: 'Address',
    updatedAt: false,
  },
);
Address.belongsTo(City, { foreignKey: 'cityId' });
Address.belongsTo(District, { foreignKey: 'districtId' });
Address.belongsTo(Ward, { foreignKey: 'wardId' });
module.exports = { Address };

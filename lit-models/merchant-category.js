const { DataTypes, Model } = require('sequelize');
const {
  facade: { DBFacade },
  providers: { DB_PROVIDER },
} = require('lit-utils');
const { Category } = require('./category');

class MerchantCategory extends Model {}

MerchantCategory.init(
  {
    merchantId: {
      field: 'merchant_id',
      type: DataTypes.INTEGER,
    },
    categoryId: {
      field: 'category_id',
      type: DataTypes.INTEGER,
    },
    createdAt: {
      field: 'created_at',
      type: DataTypes.DATE,
    },
    updatedAt: {
      field: 'updated_at',
      type: DataTypes.DATE,
    },
    id: {
      type: DataTypes.INTEGER,
      autoIncrement: true,
      primaryKey: true,
      unique: true,
      allowNull: false,
    },
  },
  {
    tableName: 'merchants_has_categories',
    sequelize: DBFacade.provider(DB_PROVIDER.SEQUELIZE).getConnection(),
    modelName: 'MerchantCategory',
  },
);

module.exports = {
  MerchantCategory,
};

Category.hasMany(MerchantCategory, { foreignKey: 'category_id' });
MerchantCategory.belongsTo(Category, { foreignKey: 'category_id' });

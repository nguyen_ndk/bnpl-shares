const { DataTypes, Model } = require('sequelize');
const {
  facade: { DBFacade },
  providers: { DB_PROVIDER },
} = require('lit-utils');

class Pincode extends Model {}

Pincode.init(
  {
    // Model attributes are defined here
    userId: {
      type: DataTypes.INTEGER,
    },
    type: {
      type: DataTypes.INTEGER,
    },
    code: {
      type: DataTypes.STRING,
    },
    updatedAt: {
      type: DataTypes.DATE,
    },
    createdAt: {
      type: DataTypes.DATE,
    },
    id: {
      type: DataTypes.INTEGER,
      autoIncrement: true,
      primaryKey: true,
      unique: true,
      allowNull: false,
    },
  },
  {
    tableName: 'pincode',
    sequelize: DBFacade.provider(DB_PROVIDER.SEQUELIZE).getConnection(),
    modelName: 'Pincode',
  },
);

module.exports = {
  Pincode,
};

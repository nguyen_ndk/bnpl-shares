const { DataTypes, Model } = require('sequelize');
const {
  facade: { DBFacade },
  providers: { DB_PROVIDER },
} = require('lit-utils');
const { Staff } = require('./staff');

class MerchantStaff extends Model {}

MerchantStaff.init(
  {
    merchantId: {
      type: DataTypes.INTEGER,
      field: 'merchant_id',
    },
    staffId: {
      type: DataTypes.INTEGER,
      field: 'staff_id',
    },
    createdAt: {
      field: 'created_at',
      type: DataTypes.DATE,
    },
    updatedAt: {
      field: 'updated_at',
      type: DataTypes.DATE,
    },
    deletedAt: {
      field: 'deleted_at',
      type: DataTypes.DATE,
    },
    id: {
      type: DataTypes.INTEGER,
      autoIncrement: true,
      primaryKey: true,
      unique: true,
      allowNull: false,
    },
  },
  {
    tableName: 'merchants_has_staffs',
    sequelize: DBFacade.provider(DB_PROVIDER.SEQUELIZE).getConnection(),
    paranoid: true,
    modelName: 'MerchantStaff',
  },
);

Staff.hasMany(MerchantStaff, { foreignKey: 'staffId' });
MerchantStaff.belongsTo(Staff, { foreignKey: 'staffId' });

module.exports = {
  MerchantStaff,
};

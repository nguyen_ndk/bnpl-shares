const { DataTypes, Model } = require('sequelize');
const {
  facade: { DBFacade },
  providers: { DB_PROVIDER },
} = require('lit-utils');
class KalapaResponse extends Model {}

KalapaResponse.init(
  {
    id: {
      type: DataTypes.INTEGER,
      autoIncrement: true,
      primaryKey: true,
      unique: true,
      allowNull: false,
    },
    kycResultId: {
      type: DataTypes.INTEGER,
      field: 'kyc_result_id',
    },
    careerName: {
      type: DataTypes.STRING(255),
      field: 'career_name',
    },
    careerDob: {
      type: DataTypes.STRING(255),
      field: 'career_dob',
    },
    careerAddr: {
      type: DataTypes.STRING(255),
      field: 'career_addr',
    },
    careerGender: {
      type: DataTypes.STRING(255),
      field: 'career_gender',
    },
    verified: {
      type: DataTypes.BOOLEAN,
      field: 'verified',
    },
    idCardInfoId: {
      type: DataTypes.STRING(255),
      field: 'id_card_info_id',
    },
    idCardInfoName: {
      type: DataTypes.STRING(255),
      field: 'id_card_info_name',
    },
    idCardInfoAddress: {
      type: DataTypes.STRING(255),
      field: 'id_card_info_address',
    },
    idCardInfoDob: {
      type: DataTypes.STRING(255),
      field: 'id_card_info_dob',
    },
    idCardInfoHome: {
      type: DataTypes.STRING(255),
      field: 'id_card_info_home',
    },
    idCardInfoExpiredDate: {
      type: DataTypes.STRING(255),
      field: 'id_card_info_expired_date',
    },
    idCardInfoDocumentType: {
      type: DataTypes.STRING(255),
      field: 'id_card_info_document_type',
    },
    idCardInfoGender: {
      type: DataTypes.STRING(255),
      field: 'id_card_info_gender',
    },
    selfieCheckMatched: {
      type: DataTypes.BOOLEAN,
      field: 'selfie_check_matched',
    },
    selfieCheckMatchingScore: {
      type: DataTypes.FLOAT(3),
      field: 'selfie_check_matching_score',
    },
    selfieCheckErrorMsg: {
      type: DataTypes.STRING(255),
      field: 'selfie_check_error_msg',
    },
    selfieCheckErrorCode: {
      type: DataTypes.INTEGER,
      field: 'selfie_check_error_code',
    },
    fraudCheckFacesMatched: {
      type: DataTypes.INTEGER(3),
      field: 'fraud_check_faces_matched',
    },
    fraudCheckFloatingMark: {
      type: DataTypes.BOOLEAN,
      field: 'fraud_check_floating_mark',
    },
    fraudCheckScreenPhoto: {
      type: DataTypes.STRING(255),
      field: 'fraud_check_screen_photo',
    },
    fraudCheckBlacklist: {
      type: DataTypes.BOOLEAN,
      field: 'fraud_check_blacklist',
    },
    fraudCheckAbnormalText: {
      type: DataTypes.BOOLEAN,
      field: 'fraud_check_abnormal_txt',
    },
    fraudCheckAbnormalFace: {
      type: DataTypes.BOOLEAN,
      field: 'fraud_check_abnormal_face',
    },
    fraudCheckCornerCut: {
      type: DataTypes.BOOLEAN,
      field: 'fraud_check_corner_cut',
    },
    score: {
      type: DataTypes.STRING(10),
      field: 'score',
    },
    numOrg: {
      type: DataTypes.INTEGER(3),
      field: 'num_org',
    },
    percentile: {
      type: DataTypes.INTEGER(3),
      field: 'percentile',
    },
    numberOfLoans: {
      type: DataTypes.INTEGER(11),
      field: 'number_of_loans',
    },
    hasBadDebt: {
      type: DataTypes.BOOLEAN,
      field: 'has_bad_debt',
    },
    hasLatePayment: {
      type: DataTypes.BOOLEAN,
      field: 'has_late_payment',
    },
    medicalNumber: {
      type: DataTypes.STRING(255),
      field: 'medical_number',
    },
    isCurrentEmployed: {
      type: DataTypes.BOOLEAN,
      field: 'is_current_employed',
    },
    salaryLevel: {
      type: DataTypes.INTEGER(11),
      field: 'salary_level',
    },
    idSelfieUrl: {
      type: DataTypes.STRING,
      field: 'id_selfie_url',
    },
    antifraudUrl: {
      type: DataTypes.STRING,
      field: 'antifraud_url',
    },
    creditUrl: {
      type: DataTypes.STRING,
      field: 'credit_url',
    },
    scoreUrl: {
      type: DataTypes.STRING,
      field: 'score_url',
    },
    createdAt: {
      type: DataTypes.DATE,
      allowNull: false,
      field: 'created_at',
    },
  },
  {
    tableName: 'kalapa_responses',
    sequelize: DBFacade.provider(DB_PROVIDER.SEQUELIZE).getConnection(),
    modelName: 'KalapaResponse',
    updatedAt: false,
  },
);

module.exports = { KalapaResponse };

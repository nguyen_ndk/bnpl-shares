const { DataTypes, Model } = require('sequelize');
const {
  facade: { DBFacade },
  providers: { DB_PROVIDER },
} = require('lit-utils');
class UserNotificationChannel extends Model {}

UserNotificationChannel.init(
  {
    id: {
      autoIncrement: true,
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true,
    },
    identifier: {
      type: DataTypes.STRING,
    },
    channel: {
      type: DataTypes.STRING(50),
    },
    userId: {
      type: DataTypes.INTEGER,
      allowNull: false,
      field: 'user_id',
    },
    createdAt: {
      field: 'created_at',
      type: DataTypes.DATE,
    },
  },
  {
    tableName: 'users_notification_channels',
    sequelize: DBFacade.provider(DB_PROVIDER.SEQUELIZE).getConnection(),
    modelName: 'UserNotificationChannel',
    updatedAt: false,
  },
);
module.exports = { UserNotificationChannel };

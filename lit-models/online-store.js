const { DataTypes, Model } = require('sequelize');
const {
  facade: { DBFacade },
  providers: { DB_PROVIDER },
} = require('lit-utils');
class OnlineStore extends Model {}

OnlineStore.init(
  {
    // Model attributes are defined here
    url: {
      type: DataTypes.STRING(255),
    },
    accessKey: {
      type: DataTypes.STRING(255),
      field: 'access_key',
    },
    merchantId: {
      type: DataTypes.STRING(36),
      field: 'merchant_id',
    },
    createdAt: {
      type: DataTypes.DATE,
      field: 'created_at',
      allowNull: false,
    },
    updatedAt: {
      type: DataTypes.DATE,
      field: 'updated_at',
    },
    deletedAt: {
      type: DataTypes.DATE,
      field: 'deleted_at',
    },
    id: {
      type: DataTypes.INTEGER,
      autoIncrement: true,
      primaryKey: true,
      unique: true,
      allowNull: false,
    },
  },
  {
    tableName: 'online_stores',
    paranoid: true,
    sequelize: DBFacade.provider(DB_PROVIDER.SEQUELIZE).getConnection(),
    modelName: 'OnlineStore',
  },
);

module.exports = { OnlineStore };

const { DataTypes, Model } = require('sequelize');
const {
  facade: { DBFacade },
  providers: { DB_PROVIDER },
} = require('lit-utils');

class PGReqToPurchase extends Model {}

PGReqToPurchase.init(
  {
    id: {
      type: DataTypes.INTEGER,
      autoIncrement: true,
      primaryKey: true,
      unique: true,
      allowNull: false,
    },
    purchaseId: {
      type: DataTypes.INTEGER,
      allowNull: false,
      field: 'purchase_id',
    },
    PgReqId: {
      type: DataTypes.INTEGER,
      allowNull: false,
      field: 'pg_req_id',
    },
    installmentNumber: {
      type: DataTypes.INTEGER,
      field: 'installment_number',
    },
  },
  {
    tableName: 'pg_req_to_purchase',
    sequelize: DBFacade.provider(DB_PROVIDER.SEQUELIZE).getConnection(),
    modelName: 'PGReqToPurchase',
    timestamps: false,
  },
);

module.exports = {
  PGReqToPurchase,
};

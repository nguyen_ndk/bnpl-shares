const { DataTypes, Model } = require('sequelize');
const {
  facade: { DBFacade },
  providers: { DB_PROVIDER },
} = require('lit-utils');
class MarketingHasCategory extends Model {}

MarketingHasCategory.init(
  {
    marketingId: {
      field: 'marketing_id',
      type: DataTypes.INTEGER,
      allowNull: false,
    },
    categoriesId: {
      type: DataTypes.INTEGER,
      field: 'categories_id',
    },

    status: {
      type: DataTypes.BOOLEAN,
      field: 'status',
      defaultValue: true,
    },
    createdAt: {
      field: 'created_at',
      type: DataTypes.DATE,
    },

    updatedAt: {
      field: 'updated_at',
      type: DataTypes.DATE,
    },
    id: {
      type: DataTypes.INTEGER,
      autoIncrement: true,
      primaryKey: true,
      unique: true,
      allowNull: false,
    },
  },
  {
    tableName: 'marketing_has_categories',
    sequelize: DBFacade.provider(DB_PROVIDER.SEQUELIZE).getConnection(),
    modelName: 'MarketingHasCategory',
  },
);

module.exports = {
  MarketingHasCategory,
};

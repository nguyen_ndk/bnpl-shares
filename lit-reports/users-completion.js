const { UserCompletionRepo, KycResultRepo } = require('lit-repositories');
const _ = require('lodash');
const { eachLimit } = require('async');

module.exports = async () => {
  const cursor = UserCompletionRepo.cursor({ limit: 10 });
  let stop = false;
  const counting = {
    active: 0,
    rejected: 0,
    emailInvalid: 0,
    phoneInvalid: 0,
    idInvalid: 0,
    cardInvalid: 0,
  };
  do {
    const users = await cursor.nextOffSet();
    if (!_.isEmpty(users)) {
      await eachLimit(users, 10, (user, next) => {
        if (user.completion === 100) {
          counting.active++;
          return next();
        }
        if (!user.emailVerified) counting.emailInvalid++;
        if (!user.phoneVerified) counting.phoneInvalid++;
        if (!user.paymentMethodAdded) counting.cardInvalid++;
        if (!user.kycPassed) {
          counting.idInvalid++;
          KycResultRepo.countByUser(user.userId).then((count) => {
            if (count > 0) counting.rejected++;
            next();
          });
        } else {
          next();
        }
      });
    } else {
      stop = true;
    }
    if (cursor.noMore) {
      stop = true;
    }
  } while (!stop);
  return counting;
};

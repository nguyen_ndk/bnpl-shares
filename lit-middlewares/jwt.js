const passport = require('passport');
const httpError = require('http-errors');
const _ = require('lodash');
const {
  facade: { Log },
} = require('lit-utils');
const { UserRepo } = require('lit-repositories');
const { AUTH_TOKEN_SECRET, errorTags } = require('lit-constants');

const JWTstrategy = require('passport-jwt').Strategy;
const ExtractJWT = require('passport-jwt').ExtractJwt;

passport.use(
  new JWTstrategy(
    {
      secretOrKey: AUTH_TOKEN_SECRET,
      jwtFromRequest: ExtractJWT.fromAuthHeaderAsBearerToken(),
      passReqToCallback: true,
    },
    async (req, token, done) => {
      try {
        const user = await UserRepo.findByEmail(token.email);
        // if (!_.get(user,'Session') || _.get(user,'Session').sessionId !== token.sessionId){
        //   return done(httpError(401, errorTags.TOKEN_EXPIRED))
        // }
        if (user.email !== token.email) {
          return done(httpError(401, errorTags.EMAIL_CHANGED));
        }

        Log.info(`USER_REQUEST path ${req.path} user ${user.id}`, {
          payload: req.body,
          params: req.params,
          query: req.query,
        });
        return done(null, user);
      } catch (error) {
        done(error);
      }
    },
  ),
);

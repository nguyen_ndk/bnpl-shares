const createError = require('http-errors');

const { AUTH, errorTags } = require('lit-constants');

const extractToken = (req) => {
  if (req.headers.authorization && req.headers.authorization.split(' ')[0] === 'Bearer') {
    return req.headers.authorization.split(' ')[1];
  } else if (req.query && req.query.token) {
    return req.query.token;
  }
  return null;
};

module.exports = async (req, res, next) => {
  const token = extractToken(req);
  if (token !== AUTH.APP_FLIER_TOKEN) return next(createError(401, errorTags.UNAUTHORIZED));
  next();
};

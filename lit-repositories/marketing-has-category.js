const { MarketingHasCategory } = require("lit-models");
const Base = require("./base");
class MarketingHasCategoryRepo extends Base {
  constructor(props) {
    super(props);
  }
}
module.exports = new MarketingHasCategoryRepo(MarketingHasCategory);

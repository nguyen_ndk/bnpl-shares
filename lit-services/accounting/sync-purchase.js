const { PurchaseRepo, PurchaseAccountingRepo } = require('lit-repositories');
const {
  facade: { Accounting, Log },
} = require('lit-utils');
const getAuth = require('./get-auth');
const _updateFinanceIds = (financeIds, purchaseId) => {
  PurchaseAccountingRepo.bulkCreate(financeIds).catch((e) => {
    Log.error(`ACCOUNTING_SYNC_PURCHASE UPDATE_PURCHASE_FINANCE_ID PURCHASE ${purchaseId}`, e);
  });
};

const syncPurchase = (purchaseId, installment) => {
  let auth;
  getAuth()
    .then((result) => {
      auth = result;
      return PurchaseRepo.findByIdForAccounting(purchaseId);
    })
    .then((purchase) => Accounting.syncPurchase(purchase, [installment], auth))
    .then(({ financeIds }) => {
      _updateFinanceIds(financeIds, purchaseId);
      return PurchaseRepo.markSyncAccounting(purchaseId);
    })
    .catch((e) => {
      Log.error(`ACCOUNTING_SYNC_PURCHASE PURCHASE ${purchaseId}`, e);
    });
};

module.exports = syncPurchase;

const {
  helpers,
  facade: { DBFacade, Log },
} = require('lit-utils');
const { PurchaseRepo } = require('lit-repositories');
const _ = require('lodash');

const updateFailedInstallments = require('../update-failed-installments');
const { Email } = require('lit-notifications');

const updateUserCredit = require('../update-user-credit-v2');
const { CREDIT_HISTORY_TYPE } = require('lit-constants');
const unSuspendUser = require('../unsuspend-user');
const addPurchaseLinePayment = require('../add-purchase-line-payment');
const validationService = require('../validation');
const charge = require('../payment/direct/charge-v2');
const { PurchaseLineRepo } = require('lit-repositories');

class PayDueConsolidateDirect {
  static getAmount(userId, purchases) {
    return PurchaseLineRepo.getDueByUserId(
      userId,
      _.map(purchases, (p) => p.id),
    );
  }

  static getDesc(purchase, changeAmount) {
    let desc = 'Pay to  purchase:';
    _.forEach(purchase, (purchase) => {
      desc += `${purchase.id} ,`;
    });
    desc += `amount: ${changeAmount}`;
    return desc;
  }

  static validate(paymentMethod, purchases, user) {
    validationService.notFoundPM(paymentMethod);
    validationService.pmBelongToUser(paymentMethod, user);
    _.forEach(purchases, (purchase) => {
      validationService.purchaseUnprocessableState(purchase);
      validationService.purchaseBelongToUser(purchase, user);
    });
  }

  static async getReturnCredit(purchases) {
    let total = 0;
    _.forEach(purchases, (purchase) => {
      const { remaining } = helpers.calcPurchaseRemaining(
        purchase.dueInstallments,
        purchase.paidInstallments,
        purchase.amount,
      );
      total += remaining;
    });

    return total;
  }

  static async update(tranId, purchases, pmId, chargeAmount, req) {
    const t = await DBFacade.transaction();
    try {
      const returnCredit = await this.getReturnCredit(purchases);
      const promises = [];
      _.forEach(purchases, (purchase) => {
        promises.push(PurchaseRepo.payInstallments(purchase, purchase.dueInstallments, t));
        promises.push(updateFailedInstallments(purchase.id, t, tranId));
      });
      await Promise.all(promises);
      await t.commit();
      _.forEach(purchases, (purchase) => {
        updateUserCredit(req.user.id, returnCredit, purchase.id, CREDIT_HISTORY_TYPE.CREDIT);
        Email.sendPaymentSuccess(req.user.email, returnCredit, chargeAmount, purchase);
        addPurchaseLinePayment(purchase, chargeAmount);
      });
      unSuspendUser(req.user);
      const getPurchasePromises = [];
      _.forEach(purchases, (purchase) => {
        getPurchasePromises.push(PurchaseRepo.findById(purchase.id));
      });
      const results = await Promise.all(getPurchasePromises);
      const data = _.map(results, (result) => PurchaseRepo.getPublicData(result));
      return { data, needOtp: false };
    } catch (e) {
      await t.rollback();
      Log.error(`PAY_CONSOLIDATE_DIRECT UPDATE_PAY_NOW USER ${req.user.id}`, e);
    }
  }

  static processPayment(req, chargeAmount, pm, desc) {
    return charge(req, chargeAmount, pm, desc);
  }
}

module.exports = PayDueConsolidateDirect;

const _ = require('lodash');
const moment = require('moment');
const httpError = require('http-errors');
const {
  facade: { KycFacade, Log },
  providers: { KYC_PROVIDER },
} = require('lit-utils');
const saveCounting = require('../save-counting');
const saveResponse = require('../save-reponse');
const uploadResponse = require('../upload-response');
const { errorTags, KYC, KALAPA, USER, DEFAULT_DATE_FORMAT } = require('lit-constants');
const { UserRepo } = require('lit-repositories');

const provider = KYC_PROVIDER.KALAPA;

/**
 * @param data
 * @param data.verified: boolean
 * @param data.selfieCheck.matched: boolean
 * @param data.faces_matched: integer
 * @param data.fraudCheck.floatingMark: boolean
 * @param data.cornerCut: boolean
 * @param data.idCardInfo.documentType: string
 * @returns {{result: boolean}|{result: boolean, errorCode: null}}
 */
const verify = (data) => {
  const id = _.get(data, 'idCardInfo.id');
  if (!id) {
    throw httpError(400, errorTags.INVALID_ID);
  }
  if (_.has(data, 'verified') && !data.verified) {
    throw httpError(400, errorTags.ID_NOT_VERIFIED);
  }

  //Check if selfie and id card are matching
  const matched = _.get(data, 'selfieCheck.matched');
  if (!matched) {
    throw httpError(400, errorTags.FAIL_TO_MATCH_SELFIE_WITH_ID_CARD);
  }

  //Check if the selfie used has been used somewhere else
  const fraudCheck = _.get(data, 'fraudCheck', {});
  if (fraudCheck.facesMatched > KALAPA.UNIQUE_PHOTO_MAX_QTY) {
    throw httpError(400, errorTags.MORE_THAN_ONE_FACE_MATCHED);
  }

  if (fraudCheck.cornerCut) {
    throw httpError(400, errorTags.DEACTIVATED_ID);
  }
  const documentType = _.get(data, 'idCardInfo.documentType');
  const floatingMark = _.get(data, 'fraudCheck.floatingMark');
  if (documentType === KALAPA.DOCUMENT_TYPE.CMDB && floatingMark === false) {
    throw httpError(400, errorTags.FLOATING_MASK);
  }
};

const _jsonPath = (userId, uuid) => `user-${userId}/${uuid}/id_plus_response.json`;

module.exports = async (idPhoto, selfie, userId, uuid) => {
  const { data, api } = await KycFacade.provider(provider).getUserInfoByIDImage(idPhoto, selfie);
  const mapped = KycFacade.provider(provider).mappingIdInfo(data);
  const file = await uploadResponse(data, _jsonPath(userId, uuid));
  mapped.idSelfieUrl = file.Location;
  const response = await saveResponse.add(mapped, provider, userId);
  let error, userData;
  try {
    verify(data);
    const gender = _.get(data, 'idCardInfo.gender');
    const birthday = _.get(data, 'idCardInfo.dob');
    userData = {
      gender: KALAPA.GENDER_MAPPING[gender] ? KALAPA.GENDER_MAPPING[gender] : USER.GENDER.OTHER,
      birthday: moment(birthday, 'DD-MM-YYYY').format(DEFAULT_DATE_FORMAT),
      address: _.get(data, 'idCardInfo.address'),
    };
    userData.gen = UserRepo.getGen(userData.birthday);
  } catch (e) {
    error = e;
  }
  saveCounting(api, provider, userId, _.get(error, 'message', KYC.COUNTING_STATUS.SUCCESS));
  if (error) return { passed: false, resId: response.id, errorMsg: error.message };

  return { id: _.get(data, 'idCardInfo.id'), userData, data, resId: response.id, passed: true };
};

const { KycReqCountingRepo } = require('lit-repositories');
const {
  facade: { Log },
} = require('lit-utils');

module.exports = (api, provider, userId, status) => {
  const saveData = {
    userId,
    api,
    provider,
    status,
  };
  KycReqCountingRepo.create(saveData).catch((e) => {
    Log.error('SAVE_KYC_RESPONSE', saveData, e);
  });
};

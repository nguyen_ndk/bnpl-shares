const _ = require('lodash');
const { PaymentGatewayRequestRepo, PaymentMethodRepo } = require('lit-repositories');
const accountingSyncPaymentGatewayFee = require('./accounting/sync-payment-gateway-fee');
const { PAYMENT_GATEWAY_REQUEST } = require('lit-constants');

const addCardType = [PAYMENT_GATEWAY_REQUEST.TYPE.ADD_PAYMENT_METHOD];

const _syncAccounting = (req, pm) => {
  accountingSyncPaymentGatewayFee(
    req.amount,
    pm,
    req.userId,
    `purchase#${req.purchaseId} installment#${req.installment}`,
  );
};

const _getPmAndSyncAccounting = (req) => {
  PaymentMethodRepo.findById(req.paymentMethodId).then((pm) => _syncAccounting(req, pm));
};
module.exports = (req, status, gatewayTranId, result) => {
  if (status === PAYMENT_GATEWAY_REQUEST.STATUS.PROCESSED) {
    if (_.includes(addCardType, req.type)) {
      _syncAccounting(req, result);
    } else {
      _getPmAndSyncAccounting(req);
    }
  }
  return PaymentGatewayRequestRepo.update(req.id, { status, gatewayTranId });
};

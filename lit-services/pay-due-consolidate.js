const httpError = require('http-errors');

const { PurchaseRepo, PaymentMethodRepo } = require('lit-repositories');
const { errorTags, PAYMENT_METHOD } = require('lit-constants');
const processors = require('./payment-processor');

const _getProcessor = (paymentMethodType) => {
  switch (paymentMethodType) {
    case PAYMENT_METHOD.TYPE.ATM:
      return processors.PayDueConsolidateOtp;
    case PAYMENT_METHOD.TYPE.CREDIT_CARD:
    case PAYMENT_METHOD.TYPE.DEBIT_CARD:
      return processors.PayDueConsolidateDirect;
    default:
      throw httpError(400, errorTags.INVALID_PAYMENT_TYPE);
  }
};

module.exports = async (req) => {
  const promises = [PaymentMethodRepo.findById(req.paymentMethodId), PurchaseRepo.findByIds(req.purchaseIds)];
  const [pm, purchases] = await Promise.all(promises);
  const processor = _getProcessor(pm.type);
  processor.validate(pm, purchases, req.user);

  const chargeAmount = await processor.getAmount(req.user.id, purchases);

  if (chargeAmount === 0) {
    throw httpError(422, errorTags.COMPLETED_PURCHASE);
  }
  const desc = await processor.getDesc(purchases, chargeAmount, req.installmentNumber);
  const result = await processor.processPayment(req, chargeAmount, pm, desc);
  return processor.update(result, purchases, pm.id, chargeAmount, req);
};

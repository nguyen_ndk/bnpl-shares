const { PurchaseRepo } = require('lit-repositories');
const {
  facade: { DBFacade, QueueFacade, Log },
} = require('lit-utils');
const { PURCHASE_PAYMENT_STATUS, RETRY, UPDATE_DB_QUEUE } = require('lit-constants');
// Retry mechanism with distributed queue for data consistency, please refactor if you find less cumbersome solutions
// Please don't refactor with solutions that their database stay in same the server with code, what will happened if the
// server between retries
const sendRetryToQueue = (context) => {
  Log.info('HANDLE_PURCHASE_ERROR SEND_TO_QUEUE', context);
  context.handler = 'handle-purchase-error';
  const delaySeconds = RETRY.UPDATE_PURCHASE_ERROR.DELAY * context.retryTimes;
  QueueFacade.send(JSON.stringify(context), {
    delaySeconds,
    queue: UPDATE_DB_QUEUE,
  }).catch((e) => {
    Log.error('HANDLE_PURCHASE_ERROR SEND_TO_QUEUE ERROR', e, context);
  });
};
module.exports = async (merReqId, errorCode, errorMessage, retryTimes) => {
  if (retryTimes > RETRY.UPDATE_PURCHASE_ERROR.MAX) return;
  const t = await DBFacade.transaction();
  try {
    const purchase = await PurchaseRepo.findByMerchantRequestIdForUpdate(merReqId, { transaction: t });
    if (purchase.paymentStatus !== PURCHASE_PAYMENT_STATUS.INITIALIZED) {
      await t.rollback();
      return;
    }
    const saveData = {
      errorCode,
      errorMessage,
      paymentStatus: PURCHASE_PAYMENT_STATUS.FAILED,
    };
    await PurchaseRepo.update(purchase.id, saveData, { getModel: false, transaction: t });
    await t.commit();
  } catch (e) {
    Log.error('HANDLE_PURCHASE_ERROR ERROR', e, merReqId, retryTimes, errorCode, errorMessage);
    sendRetryToQueue({ merReqId, errorCode, errorMessage, retryTimes: retryTimes + 1 });
  }
};

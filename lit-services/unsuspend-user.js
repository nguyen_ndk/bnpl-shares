const { PurchaseRepo } = require('lit-repositories');
const updateUserStatus = require('./update-user-status');
const { USER } = require('lit-constants');
const {
  facade: { Log },
} = require('lit-utils');
module.exports = (user) => {
  if (user.status !== USER.STATUS.SUSPENDED_FOR_FAILED_PAYMENT) return;
  PurchaseRepo.countHasInstallmentFailedByUserId(user.id)
    .then((countHasInstalmentFailedPurchase) => {
      if (countHasInstalmentFailedPurchase == 0) {
        return updateUserStatus(user.id, USER.STATUS.ACTIVE);
      }
      return;
    })
    .catch((e) => {
      Log.error(`UNSUSPEND_USER user ${user.id}`, e);
    });
};

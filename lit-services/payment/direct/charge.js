const uuid = require('uuid');
const {
  facade: { PaymentGatewayFacade, Log },
} = require('lit-utils');
const { DEFAULT_CURRENCY, PAYMENT_GATEWAY_REQUEST } = require('lit-constants');
const { PaymentGatewayRequestRepo } = require('lit-repositories');
const accountingSyncPaymentGatewayFee = require('../../accounting/sync-payment-gateway-fee');
module.exports = async (pm, amount, desc, userId, purchaseId, installmentId) => {
  const context = {
    tranId: uuid.v4(),
    batchTranId: uuid.v4(),
    userId,
  };
  let error;
  try {
    // TODO remove this, receive the gateway from payment method
    var { gatewayTranId } = await PaymentGatewayFacade.provider(pm.gateway).charge(
      pm.bankToken,
      amount,
      DEFAULT_CURRENCY,
      desc,
      context,
    );
  } catch (e) {
    error = e;
  }
  const logData = {
    type: PAYMENT_GATEWAY_REQUEST.TYPE.CHARGE,
    gateway: pm.gateway,
    transactionId: context.tranId,
    batchTranId: context.batchTranId,
    userId,
    status: error ? PAYMENT_GATEWAY_REQUEST.STATUS.FAILED : PAYMENT_GATEWAY_REQUEST.STATUS.PROCESSED,
    gatewayTranId,
    amount,
    paymentMethodId: pm.id,
    purchaseId,
    installmentId,
  };
  await PaymentGatewayRequestRepo.create(logData).catch((error) => {
    Log.error('PAYMENT_GATEWAY_CHARGE LOG', { logData }, { error });
  });
  if (error) {
    throw error;
  }
  accountingSyncPaymentGatewayFee(amount, pm, userId, desc);
  return context.tranId;
};

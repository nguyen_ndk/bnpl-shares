const { PurchaseRepo, PaymentMethodRepo, UserRepo } = require('lit-repositories');
const { PayOtp } = require('../../payment-processor');

module.exports = async (extractedData, paymentGatewayReq) => {
  const purchaseId = paymentGatewayReq.purchaseId;
  const p = await PurchaseRepo.findById(purchaseId);
  const [pm, user] = await Promise.all([
    PaymentMethodRepo.findById(paymentGatewayReq.paymentMethodId),
    UserRepo.findById(paymentGatewayReq.userId),
  ]);

  return PayOtp.webhook(paymentGatewayReq, p, pm.id, user);
};

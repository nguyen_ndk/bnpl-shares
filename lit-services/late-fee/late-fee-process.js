const {
  facade: { DBFacade, Log },
} = require('lit-utils');
const { PurchaseInstallmentRepo, LateFeeRepo } = require('lit-repositories');
const { LATE_FEE, DEFAULT_DATETIME_FORMAT, QUERY_METHOD_MAPPING, PURCHASE } = require('lit-constants');
const moment = require('moment');
const { LateFee, User, Purchase } = require('lit-models');

const _ = require('lodash');
const createPurchaseLine = require('../purchase-line/create-purchase-line');
const async = require('async');
const { Email } = require('lit-notifications');

/*
 GET LIST OF LATE PAYMENT ON PURCHASE INSTALLMENT
 */
const getLatePurchaseInstallement = async (deadlineHours) => {
  try {
    const conditions = {
      group: ['purchaseId'],
      include: [
        {
          model: Purchase,
          include: [
            {
              model: User,
            },
            {
              model: LateFee,
              separate: true,
              order: [['feeNumber', 'DESC']], //Very Important to get the last late_fee count,
            },
          ],
        },
      ],
      where: {
        status: {
          [QUERY_METHOD_MAPPING.IN]: [PURCHASE.INSTALLMENT_STATUS.SOFT_FAILED, PURCHASE.INSTALLMENT_STATUS.HARD_FAILED],
        }, //PAYMENT FAILED
        dueDate: {
          [QUERY_METHOD_MAPPING.LT]: moment().subtract(deadlineHours, 'hours').format(DEFAULT_DATETIME_FORMAT),
        }, //DATETIME WHEN THE LATE FEE SHOULD START
        installmentNumber: { [QUERY_METHOD_MAPPING.GT]: 1 }, //STARTING ON INSTALLMENT NUMBER 2 ONLY
      },
    };

    const purchaseInstallments = await PurchaseInstallmentRepo.findAll(conditions);

    Log.info('GET_LATE_PURCHASE_INSTALLMENT');

    return purchaseInstallments;
  } catch (e) {
    Log.error('GET_LATE_PURCHASE_INSTALLMENT', e);
  }
};

/*
  GET THE CONFIG (FEE AMOUNT) OF THE PURCHASE
 */
const getConfigLine = (purchaseInstallement) => {
  const purchaseAmount = purchaseInstallement.Purchase.amount;
  var result = false;
  _.find(LATE_FEE.CONFIG, (LFC) => {
    if (
      purchaseAmount >= LFC[LATE_FEE.CONFIG_KEY['purchaseAmountMin']] &&
      purchaseAmount <= LFC[LATE_FEE.CONFIG_KEY['purchaseAmountMax']]
    ) {
      return (result = LFC);
    }
  });

  return result;
};

// feeAmount, feeNumber, LFC
const saveLateFee = async (purchaseInstallment, options) => {
  const t = await DBFacade.transaction();

  try {
    var purchaseAmount = purchaseInstallment.Purchase.amount;
    var constants = LATE_FEE.CONFIG_KEY;
    constants.values = options.LFC;
    options.percentage = getPercentage(options.feeAmount, purchaseAmount);
    options.cumulativePercentage = getPercentage(options.cumulativeAmount, purchaseAmount);

    const saveData = {
      purchaseId: purchaseInstallment.Purchase.id,
      userId: purchaseInstallment.Purchase.userId,
      amount: options.feeAmount,
      percentage: options.percentage,
      dueDate: moment().format(DEFAULT_DATETIME_FORMAT),
      feeNumber: options.feeNumber,
      lateDaysFrequency: options.LFC[LATE_FEE.CONFIG_KEY['lateDaysFrequency']],
      constants: JSON.stringify(constants),
      cumulativeAmount: options.cumulativeAmount,
      cumulativePercentage: options.cumulativePercentage,
    };

    const lateFee = await LateFeeRepo.create(saveData);

    await createPurchaseLine(lateFee);

    await t.commit();
    Log.info('SAVE_LATE_FEE');

    Email.sendLateFee(purchaseInstallment.Purchase.User, lateFee);

    return lateFee;
  } catch (e) {
    Log.error('SAVE_LATE_FEE_ERROR ', e);
    await t.rollback();
  }
};

const isAfterExpectedDueDate = (expectedDueDate, dueDate) => {
  return expectedDueDate.isAfter(dueDate);
};

const isPercentageLimitReached = (cumulativePercentage, maxPercentagePurchaseFeeAmount) => {
  if (parseFloat(cumulativePercentage) < parseFloat(maxPercentagePurchaseFeeAmount)) {
    return false;
  }

  return true;
};

const getPercentage = (amount, totalAmount) => {
  return _.round((parseFloat(amount) / parseFloat(totalAmount)) * 100, 10);
};

module.exports = async (mode) => {
  //Get Late Purchase Installment with failed payment
  const purchaseInstallments = await getLatePurchaseInstallement(LATE_FEE.DEADLINE_HOURS);

  //Foreach Installment failed
  await async.eachLimit(purchaseInstallments, 10, async (purchaseInstallment) => {
    //Late Fee Config per Installment
    var LFC = getConfigLine(purchaseInstallment);

    //Get Existing late fees
    var existingLateFees = purchaseInstallment.Purchase.LateFees;

    // console.log(LFC)
    var firstFee = true;
    //IF EXISTING LATE FEE = NOT FIRST TIME
    if (existingLateFees.length > 0) firstFee = false;

    //Total amount of the purchase
    var purchaseAmount = purchaseInstallment.Purchase.amount;

    //Fee amount for late fee
    var feeAmount = LFC[LATE_FEE.CONFIG_KEY['feeAmount']];
    var feeNumber = 1;

    //VERY FIRST LATE FEE
    if (firstFee) {
      saveLateFee(purchaseInstallment, {
        feeAmount: feeAmount,
        cumulativeAmount: feeAmount,
        feeNumber: feeNumber,
        LFC: LFC,
      });

      return true;
    }

    var existingCumulativePercentage = existingLateFees[0].cumulativePercentage;
    var maxPercentagePurchaseFeeAmount = LFC[LATE_FEE.CONFIG_KEY['maxPercentagePurchaseFeeAmount']];

    //If its time to apply late feee
    if (
      (mode == 'nolimit' && !isPercentageLimitReached(existingCumulativePercentage, maxPercentagePurchaseFeeAmount)) ||
      (isAfterExpectedDueDate(
        moment().subtract(existingLateFees[0].lateDaysFrequency, 'days'),
        existingLateFees[0].dueDate,
      ) &&
        //If the late fee amount didnt reach the limit
        !isPercentageLimitReached(existingCumulativePercentage, maxPercentagePurchaseFeeAmount))
    ) {
      var cumulativePercentage = _.round(
        parseFloat(existingCumulativePercentage) + parseFloat((feeAmount / parseFloat(purchaseAmount)) * 100),
        10,
      );

      //Last percentage
      if (isPercentageLimitReached(cumulativePercentage, maxPercentagePurchaseFeeAmount)) {
        const percentageLeft = _.round(
          parseFloat(maxPercentagePurchaseFeeAmount) - parseFloat(existingCumulativePercentage),
          10,
        );

        if (percentageLeft > 0.0) {
          feeAmount = _.round(purchaseAmount * (percentageLeft / 100));
        }
      }

      var cumulativeAmount = existingLateFees[0].cumulativeAmount + feeAmount;

      saveLateFee(purchaseInstallment, {
        feeAmount: feeAmount,
        cumulativeAmount: cumulativeAmount,
        feeNumber: ++existingLateFees[0].feeNumber,
        LFC: LFC,
      });

      return true;
    }
  });
};

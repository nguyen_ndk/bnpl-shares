const httpError = require('http-errors');
const {
  facade: { Log },
} = require('lit-utils');
const { errorTags } = require('lit-constants');
const { UserRepo } = require('lit-repositories');
module.exports = async (userId, credit, transaction) => {
  const logTag = `SAVE_USER_CREDIT user: ${userId}`;
  Log.info(`${logTag} START`);
  const saveData = { credit };
  const res = await UserRepo.update(userId, saveData, { getModel: false, transaction });
  if (!res) {
    Log.error(logTag, saveData);
    throw httpError(500, errorTags.SERVER_ERROR);
  }
};

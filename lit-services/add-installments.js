const { PurchaseRepo, PurchaseInstallmentRepo, PurchaseLineRepo } = require('lit-repositories');
const { eachLimit } = require('async');

const _getAmount = (purchase) =>
  PurchaseRepo.calcPurchaseInstallmentAmount(purchase.amount, purchase.totalInstallments);

const bulkCreate = async (purchase, installmentNumber, pmId, tranId, transaction) => {
  const amount = _getAmount(purchase);
  const installmentData = PurchaseInstallmentRepo.buildInstallmentData(
    purchase,
    amount,
    pmId,
    installmentNumber,
    tranId,
  );
  const installments = await PurchaseInstallmentRepo.bulkCreate(installmentData, { transaction });
  await eachLimit(installments, 4, async (installment) =>
    PurchaseLineRepo.addInstallment(installment, purchase.userId, { transaction }),
  );
};

const add = async (purchase, amount, pmId, tranId, transaction) => {
  const installment = await PurchaseInstallmentRepo.addSuccessInstallment(purchase, amount, pmId, tranId, transaction);
  await PurchaseLineRepo.addInstallment(installment, purchase.userId, { transaction });
};

module.exports = {
  bulkCreate,
  add,
};

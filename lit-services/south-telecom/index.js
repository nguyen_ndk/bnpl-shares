const request = require('request')
require('dotenv/config')

// TO DO 
// Move to utils

const addLeads = (name, phone, email, address) => {
  let bodyData = {
    name,
    phone,
    email,
    address
  }
  return new Promise((resolve, reject)=>{
    request.post({
      url: process.env.SOUTH_TELECOM_URL+'/leads',
      headers: {
        'content-type': 'application/json',
        apikey: process.env.SOUTH_TELECOM_APIKEY_LEADS
      },
      body: JSON.stringify(bodyData),
    }, (err, res, body)=>{
      if (err){
        reject(null)
      }
      resolve(body)
    })
  })
}

const createCustomer = (id, name, phone, email, urls, type) => {
  let bodyData = {
    id,
    name,
    phone,
    email,
    urls,
    type
  }

  return new Promise((resolve, reject)=>{
    request.post({
      url: process.env.SOUTH_TELECOM_URL+'/customers',
      headers: {
        'content-type': 'application/json',
        apikey: process.env.SOUTH_TELECOM_APIKEY_CUSTOMER
      },
      body: JSON.stringify(bodyData),
    }, (err, res, body)=>{
      if (err){
        reject(err)
      }
      resolve(body)
    })
  })
}


const updateCustomers = (id, name, phone, email, urls, type) => {
  let bodyData = {
    name,
    phone,
    email,
    urls,
    type
  }

  return new Promise((resolve, reject)=>{
    request.post({
      url: process.env.SOUTH_TELECOM_URL+`/customers/${id}`,
      headers: {
        'content-type': 'application/json',
        apikey: process.env.SOUTH_TELECOM_APIKEY_CUSTOMER
      },
      params: JSON.stringify(bodyData),
    }, (err, res, body)=>{
      if (err){
        reject(err)
      }
      resolve(body)
    })
  })
}

module.exports = { addLeads, createCustomer, updateCustomers }
module.exports = {
  PURCHASE_FIELDS: ['id', 'phone', 'email'],
  FIELDS_FOR_MERCHANT_VIEW: ['email', 'firstName', 'lastName'],
  GENDER: {
    MALE: 'male',
    FEMALE: 'female',
    GAY: 'gay',
    LESBIAN: 'lesbian',
    OTHER: 'other',
  },
  LANG: {
    VN: 'vn',
    EN: 'en',
  },
  PUBLIC_DATA: [
    'email',
    'phone',
    'newPhone',
    'address',
    'credit',
    'firstName',
    'lastName',
    'gender',
    'idPhotoUrl',
    'userPhotoWithIdUrl',
    'status',
    'tier',
    'UserCompletion',
    'isProcessingKYC',
    'avatarUrl',
  ],
  COMPLETION: {
    PHONE: 'phone',
    EMAIL: 'email',
    CARD: 'card',
    ID: 'id',
  },
  TIER: {
    T1: 1,
    T2: 2,
    T3: 3,
    T4: 4,
  },
  STATUS: {
    NEW: 'new',
    ACTIVE: 'active',
    SUSPENDED_FOR_FAILED_PAYMENT: 'suspend_for_failed_payment', //Suspended by cronjob (overrallbilling) if payment failed
  },
  LIMITATION: {
    FIRSTNAME_LENGTH: 100,
    LASTNAME_LENGTH: 100,
  },
  GEN: {
    GEN_X: 'gen_x',
    MILLENNIAL: 'millennial',
    GEN_Z: 'gen_z',
    OTHER: 'other',
  }
}
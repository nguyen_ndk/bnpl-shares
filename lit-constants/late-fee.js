const CONFIG = [
  // ['purchaseAmountMin', 'purchaseAmountMax', 'feeAmount', 'dayCycle', 'maxPercentagePurchaseFeeAmount'],
  [0, 1000000, 10000, 1, 20],
  [1000001, 2000000, 20000, 1, 20],
  [2000001, 3000000, 30000, 1, 20],
  [3000001, 4000000, 40000, 1, 20],
  [4000001, 999999999, 50000, 1, 20],
]

const CONFIG_KEY = {}

//Index of CONFIG (Value = Column index of CONFIG array)
CONFIG_KEY['purchaseAmountMin'] = 0
CONFIG_KEY['purchaseAmountMax'] = 1
CONFIG_KEY['feeAmount'] = 2
CONFIG_KEY['lateDaysFrequency'] = 3
CONFIG_KEY['maxPercentagePurchaseFeeAmount'] = 4

const DEADLINE_HOURS = 1 //How many hours after applying late feee


module.exports = {
  CONFIG,
  DEADLINE_HOURS,
  CONFIG_KEY
}
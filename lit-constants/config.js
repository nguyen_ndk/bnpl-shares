const awsParamStore = require('aws-param-store');
const fs = require('fs');
const dotenv = require('dotenv');
const stage = process.env.STAGE;

const prefix = `/bnpl/${stage}`;

let config = {};

if (stage !== 'local') {
  // load from Parameter Store
  let parameters = awsParamStore.getParametersByPathSync(prefix, {
    region: 'ap-southeast-1',
  });
  for (const p of parameters) {
    let paramName = p.Name.split('/').slice(-1)[0];
    config[paramName.toUpperCase()] = p.Value;
  }
} else {
  // load from local
  let envConfig = dotenv.parse(fs.readFileSync('.env'));
  config = envConfig;
}

module.exports = config;

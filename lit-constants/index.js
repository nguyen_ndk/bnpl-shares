require("module-alias/register");

const _ = require("lodash");
const { Op } = require("sequelize");
const config = require("./config");

const AUTH_TOKEN_EXPIRATION = config.AUTH_TOKEN_EXPIRATION;
const AUTH_TOKEN_SECRET = config.AUTH_TOKEN_SECRET;
const AUTH_LIT_USER_TOKEN_EXPIRATION = config.AUTH_LIT_USER_TOKEN_EXPIRATION;
const AUTH_LIT_USER_TOKEN_SECRET = config.AUTH_LIT_USER_TOKEN_SECRET;
const EMAIL_TOKEN_EXPIRATION = config.EMAIL_TOKEN_EXPIRATION;
const EMAIL_TOKEN_SECRET = config.EMAIL_TOKEN_SECRET;
const PASSWORD_TOKEN_EXPIRATION = config.PASSWORD_TOKEN_EXPIRATION;
const PASSWORD_TOKEN_SECRET = config.PASSWORD_TOKEN_SECRET;
const BASE_URL = config.BASE_URL;
const FRONT_END_BASE_URL = config.FRONT_END_BASE_URL;
const ADMIN_FE_BASE_URL = config.ADMIN_FE_BASE_URL;
const MERCHANT_FE_BASE_URL = config.MERCHANT_FE_BASE_URL;
const AUTH = {
  MERCHANT: {
    SECRET: {
      WEB: config.AUTH_MERCHANT_WEB_SECRET,
      API: config.AUTH_MERCHANT_SECRET,
      STAFF: config.AUTH_MERCHANT_STAFF_SECRET,
      POS: config.AUTH_POS_SECRET,
      STAFF_MOBILE: config.STAFF_MOBILE,
    },
    EXPIRATION: {
      WEB: config.AUTH_MERCHANT_WEB_EXPIRATION,
    },
  },
  APP_TOKEN: config.APP_TOKEN,
  APP_FLIER_TOKEN: config.APP_FLIER_TOKEN,
};

const CONFIRM_PURCHASE_QUEUE = config.CONFIRM_PURCHASE_QUEUE;
const UPDATE_DB_QUEUE = config.UPDATE_DB_QUEUE;
const DEVOPS_EMAILS = config.DEVOPS_EMAILS;
const TRANSACTION_FEE = parseInt(config.TRANSACTION_FEE);
const DEFAULT_MERCHANT_COMMISION = parseFloat(
  config.DEFAULT_MERCHANT_COMMISION
);
const YOPAYMENT = {
  MERCHANT_CODE: config.YO_MERCHANT_CODE,
  PASSCODE: config.YO_PASSCODE,
  CHECKSUM_KEY: config.YO_CHECKSUM_KEY,
  LIT_IP: config.LIT_IP,
  TRIPLE_DES_KEY: config.YO_TRIPLE_DES_KEY,
  BASE_URL: config.YO_BASE_URL,
  ERROR_CODE: {
    SUCCESS_CODE: "0",
    ERROR_SERVER_BUSY: "1000",
    VALIDATION_FAILED: "1001",
    INVALID_PARAM: "1002",
    UNAUTHENTICATED: "1007",
    INVALID_CHECKSUM: "1009",
    UNKNOWN_ERROR: "666",
  },
  LIMITATION: {
    DESCRIPTION: {
      LENGTH: 500,
    },
    TITLE: {
      LENGTH: 100,
    },
  },
};
const WINDOW_TYPE = {
  DESKTOP: "desktop",
  MOBILE: "mobile",
};

const PAYMENT_GATEWAY_REQUEST = {
  GATEWAY: {
    YO: "YOPAYMENT",
  },
  TYPE: {
    CHARGE: "charge",
    REFUND: "refund",
    CHARGE_QR_3D_SECURE: "charge_qr_3d_secure",
    CHARGE_AIO_3D_SECURE: "charge_aio_3d_secure",
    ADD_PAYMENT_METHOD: "add_payment_method",
  },
  STATUS: {
    INIT: "init",
    PROCESSED: "processed",
    FAILED: "failed",
    ABORTED: "aborted",
  },
  RETRY_MAX: 15,
};

const DEFAULT_COUNTRY = "vn";
const USER_DATA_VIEW_BY_ADMIN = [
  "email",
  "phone",
  "phone",
  "address",
  "credit",
  "tier",
  "idPhoto",
  "photoWithId",
  "firstName",
  "lastName",
  "gender",
  "id",
  "status",
  "avatarUrl",
  "UserCompletion",
];
const MERCHANT_PUBLIC_DATA = ["thumbnail", "name", "type", "Marketing"];

const PAYMENT_METHOD = {
  PUBLIC_DATA: [
    "ccCropped",
    "ccValidity",
    "cardDetail",
    "bankDetail",
    "id",
    "type",
  ],
  STATUS: {
    PRIMARY: "primary",
    SECONDARY: "secondary",
    DELETED: "deleted",
  },
  TYPE: {
    ATM: "atm",
    DEBIT_CARD: "debit_card",
    CREDIT_CARD: "credit_card",
    VIRTUAL_ACCOUNT: "virtual_account",
    E_WALLET: "e_wallet",
    NO: "nothing",
  },
};

const INSTALLMENT = {
  NUMBER: config.NUMBER_OF_INSTALLMENT,
  DUE_GAP: config.INSTALLMENT_DUE_GAP,
  PUBLIC_DATA: [
    "installmentNumber",
    "id",
    "status",
    "amount",
    "paidAt",
    "transactionId",
  ],
};

const CREDIT_HISTORY_TYPE = {
  CREDIT: "C",
  INSTALL_DEDUCT: "I",
  BLOCK: "B",
};

const ROLES = {
  USER: "user",
  LIT_USER: "lit_user",
  ADMIN: "admin",
  FINANCE: "finance",
  MERCHANT: "merchant",
  CUSTOMER_SUPPORT: "customer_support",
  MARKETING: "marketing",
};

const RETRY = {
  UPDATE_PURCHASE_ERROR: {
    MAX: 3,
    DELAY: 300,
  },
};

const USER = require("./user");

const PINCODE = {
  TYPE: {
    PHONE_VERIFICATION: 1,
  },
  EXPIRED_TIME: config.TIME_CODE_EXPIRED,
};

const PURCHASE_PAYMENT_STATUS = {
  INITIALIZED: "initialized",
  IN_PROGRESS: "in_progress",
  USER_DISPUTE: "user_dispute",
  MERCHANT_DISPUTE: "merchant_dispute",
  REFUNDED: "refunded",
  COMPLETED: "completed",
  FAILED: "failed",
  USER_CANCELLED: "user_canceled",
  SYSTEM_CANCELLED: "system_cancelled",
  INSTALLMENT_FAILED: "installment_failed",
  REFUNDED_WITH_FAILED_INSTALLMENT: "refund_with_failed_installment",
  REFUNDING: "refunding",
};

const QUERY_METHOD_MAPPING = {
  EQ: Op.eq,
  IN: Op.in,
  NOT_IN: Op.notIn,
  NE: Op.ne,
  NOT: Op.not,
  IS: Op.is,
  GT: Op.gt,
  LT: Op.lt,
  LK: Op.like,
  ILK: Op.iLike,
  SUBSTR: Op.substring,
  OR: Op.or,
  AND: Op.and,
};

const PURCHASE = {
  SOURCE: {
    ECOMMERCE: 1,
    POS: 2,
    QR: 3,
    MOBILE_POS: 4,
  },
  PUBLIC_DATA_FOR_MERCHANT_VIEW: ["amount", "id", "description", "createdAt"],
  PUBLIC_DATA: [
    "amount",
    "paymentStatus",
    "numberOfInstallments",
    "merchantId",
    "createdAt",
    "updatedAt",
    "returnUrl",
    "payFirstInstallmentAt",
    "description",
    "id",
    "PurchaseInstallments",
    "User",
    "Merchant",
    "refundedAt",
    "paidInstallments",
    "dueInstallments",
  ],
  PURCHASE_SCREEN: {
    ON_GOING: "on_going",
    COMPLETED: "completed",
  },
  INSTALLMENT_STATUS: {
    DUE: "due",
    VALIDATED: "validated",
    SOFT_FAILED: "soft_failed", //CAN RETRY TO PAY
    HARD_FAILED: "hard_failed", //FAILED FOR GOOD
    REFUNDED: "refunded",
  },
  PAYEE_AGENT: {
    MOBILE: "mobile",
    WEB: "web",
    QR: "qr",
  },
  SEARCH_TYPE: {
    IN_PROGRESS: "current",
    COMPLETED: "history",
    ALL: "all",
  },
  DELIVERY_STATUS: {
    ORDERED: "ordered",
    DELIVERED: "delivered",
  },
  MERCHANT_PAID_STATUS: {
    NOT_YET: "not_yet",
    HOLD: "hold",
    PROCESSING: "processing",
    COMPLETED: "completed",
    REFUNDED: "refunded",
  },
  LIMITATION: {
    MAX_PURCHASE_AMOUNT: parseInt(config.MERCHANT_MAX_PURCHASE_AMOUNT),
    MIN_PURCHASE_AMOUNT: parseInt(config.MERCHANT_MIN_PURCHASE_AMOUNT),
    REQUEST_ID_MAX_LENGTH: 255,
    ORDER_ID_MAX_LENGTH: 255,
    BILL_ID_MAX_LENGTH: 255,
    SIGNATURE_ID_MAX_LENGTH: 1000,
    DESC_ID_MAX_LENGTH: 1000,
  },
  ACCOUNTING_SYNC_STATUS: {
    DONE: "done",
    FAILED: "failed",
    PARTLY_DONE: "partly_done",
    NEW: "new",
  },
  ALLOW_TO_PAY_NEXT_INSTALLMENT_STATE: [PURCHASE_PAYMENT_STATUS.IN_PROGRESS],
  ALLOW_TO_PAY_FAILED_INSTALLMENT_STATE: [
    PURCHASE_PAYMENT_STATUS.INSTALLMENT_FAILED,
  ],
  DAYS_TO_SEND_INSTALLMENT_DUE_EMAIL: config.DAYS_TO_SEND_INSTALLMENT_DUE_EMAIL,
  PAY_MULTI_TYPE: {
    PAY_DUE: "pay_due",
    PAY_ALL: "pay_all",
  },
};

const PURCHASE_SEARCH_TYPE_TO_STATUS = {
  [PURCHASE.SEARCH_TYPE.IN_PROGRESS]: [
    PURCHASE_PAYMENT_STATUS.IN_PROGRESS,
    PURCHASE_PAYMENT_STATUS.USER_DISPUTE,
    PURCHASE_PAYMENT_STATUS.MERCHANT_DISPUTE,
    PURCHASE_PAYMENT_STATUS.INSTALLMENT_FAILED,
  ],
  [PURCHASE.SEARCH_TYPE.COMPLETED]: [
    PURCHASE_PAYMENT_STATUS.COMPLETED,
    PURCHASE_PAYMENT_STATUS.REFUNDED,
    PURCHASE_PAYMENT_STATUS.FAILED,
    PURCHASE_PAYMENT_STATUS.USER_CANCELLED,
    PURCHASE_PAYMENT_STATUS.SYSTEM_CANCELLED,
  ],
  [PURCHASE.SEARCH_TYPE.ALL]: [
    PURCHASE_PAYMENT_STATUS.COMPLETED,
    PURCHASE_PAYMENT_STATUS.REFUNDED,
    PURCHASE_PAYMENT_STATUS.FAILED,
    PURCHASE_PAYMENT_STATUS.USER_CANCELLED,
    PURCHASE_PAYMENT_STATUS.SYSTEM_CANCELLED,
    PURCHASE_PAYMENT_STATUS.IN_PROGRESS,
    PURCHASE_PAYMENT_STATUS.USER_DISPUTE,
    PURCHASE_PAYMENT_STATUS.MERCHANT_DISPUTE,
    PURCHASE_PAYMENT_STATUS.INSTALLMENT_FAILED,
  ],
};

const MERCHANT = {
  STATUS: {
    ACTIVE: "active",
    INACTIVE: "inactive",
  },
  TYPE: {
    ONLINE: "online",
    OFFLINE: "offline",
    ONLINE_AND_OFFLINE: "offline_and_online",
  },
  LOCATION_TYPE: {
    ONLINE: 1,
    OFFLINE: 2,
  },
  PUBLIC_DATA: [
    "thumbnail",
    "name",
    "type",
    "id",
    "status",
    "adminFeeAmount",
    "adminFeeType",
  ],
  PUBLIC_POS_DATA: [
    "thumbnail",
    "name",
    "type",
    "id",
    "status",
    "code",
    "clientId",
  ],
  AUTH_TOKEN_URL: {
    STAFF: `${BASE_URL}/me/StLo`,
    POS: "",
  },
};

const ADMIN_FEE = {
  TYPE: {
    PERCENTAGE: "percentage",
    AMOUNT: "amount",
  },
};

const MERCHANT_FINANCIAL = {
  TYPE: {
    PERCENTAGE: 1,
    AMOUNT: 2,
  },
  STATUS: {
    ACTIVE: 1,
    EXPIRED: 2,
  },
};

const MERCHANT_STAFF = {
  STATUS: {
    ACTIVE: 1,
    INACTIVE: 2,
  },
  ROLE: {
    CASHIER: 1,
    LOCATION_MANAGER: 2,
  },
  PUBLIC_DATA: ["id", "role", "merchantId", "status", "createdAt", "name"],
};

const MERCHANT_TYPE_HAVE_ONLINE = [
  MERCHANT.TYPE.ONLINE,
  MERCHANT.TYPE.ONLINE_AND_OFFLINE,
];

const LIMIT_BY_TIER = {
  [USER.TIER.T1]: {
    [PAYMENT_METHOD.TYPE.ATM]: config.ATM_TIER_1_LIMIT,
    [PAYMENT_METHOD.TYPE.CREDIT_CARD]: config.DRCR_TIER_1_LIMIT,
    [PAYMENT_METHOD.TYPE.DEBIT_CARD]: config.DRCR_TIER_1_LIMIT,
  },
  [USER.TIER.T2]: {
    [PAYMENT_METHOD.TYPE.ATM]: config.ATM_TIER_2_LIMIT,
    [PAYMENT_METHOD.TYPE.CREDIT_CARD]: config.DRCR_TIER_2_LIMIT,
    [PAYMENT_METHOD.TYPE.DEBIT_CARD]: config.DRCR_TIER_2_LIMIT,
  },
  [USER.TIER.T3]: {
    [PAYMENT_METHOD.TYPE.ATM]: config.ATM_TIER_3_LIMIT,
    [PAYMENT_METHOD.TYPE.CREDIT_CARD]: config.DRCR_TIER_3_LIMIT,
    [PAYMENT_METHOD.TYPE.DEBIT_CARD]: config.DRCR_TIER_3_LIMIT,
  },
  [USER.TIER.T4]: {
    [PAYMENT_METHOD.TYPE.ATM]: config.ATM_TIER_4_LIMIT,
    [PAYMENT_METHOD.TYPE.CREDIT_CARD]: config.DRCR_TIER_4_LIMIT,
    [PAYMENT_METHOD.TYPE.DEBIT_CARD]: config.DRCR_TIER_4_LIMIT,
  },
};

const KYC = {
  STATUS: {
    PASSED: "PASSED",
    FAILED: "FAILED",
  },
  WEIGHT: {
    HAS_LATE_PAYMENT: parseInt(config.KYC_HAS_LATE_PAYMENT_WEIGHT),
    IS_CURRENT_EMPLOYED: parseInt(config.KYC_IS_CURRENT_EMPLOYED_WEIGHT),
  },
  COUNTING_STATUS: {
    SUCCESS: "SUCCESS",
  },
  UPDATE_GAP_DAY: 1,
};

KYC.WEIGHT.TOTAL = _.reduce(KYC.WEIGHT, (total, weight) => total + weight, 0);

const ADDRESS = {
  PUBLIC_DATA: ["line1", "id"],
};
const CITY = {
  PUBLIC_DATA: ["name", "id"],
};
const DISTRICT = {
  PUBLIC_DATA: ["name", "id"],
};
const WARD = {
  PUBLIC_DATA: ["name", "id"],
};
const COMPANY = {
  STATUS: {
    ACTIVE: 1,
  },
  PUBLIC_DATA: ["name"],
};
const MERCHANT_LIST = {
  PUBLIC_DATA: ["companyId", "id", "name"],
};

const MOBILE_POS_PURCHASE = {
  STATUS: {
    INIT: 1,
    USER_APPROVED: 2,
    MERCHANT_APPROVED: 3,
    MERCHANT_REJECTED: 4,
    USER_FAILED_TO_CONFIRM: 5,
  },
  PUBLIC_DATA: [
    "id",
    "amount",
    "merchantDesc",
    "createdAt",
    "status",
    "transactionId",
    "userDesc",
    "User",
  ],
  NOTIFICATION_TYPE: {
    USER_CONFIRM: "user_confirm",
    USER_FAILED_TO_PAY: "user_failed_to_pay",
  },
};

const POS_DEVICE = {
  STATUS: {
    INIT: 1,
    ACTIVE: 2,
  },
};

const CONFIGURATION = {
  LIMITATION: {
    MAX_VALUE: 1000,
    MAX_NAME: 255,
  },
  FIELD_NAME: {
    XERO_REFRESH_TOKEN: "xero_refresh_token",
    XERO_ACCESS_TOKEN: "xero_access_token",
    XERO_EXPIRED_AT: "xero_expired_at",
    XERO_TOKEN_SET: "xero_token_set",
  },
};

const SMS_LOGS = {
  TYPE: {
    OTP: 1,
    PAYMENT: 2,
    REFUND: 3,
    LATE_FEE: 4,
  },
};

const MARKETING_CATEGORY = {
  PUBLIC_DATA: ["id", "slug", "name"],
};

const MARKETING = {
  MERCHANT_TYPE: {
    OFFLINE: "offline",
    ONLINE: "online",
    OFFLINE_AND_ONLINE: "offline_and_online",
  },
  STATUS: {
    ENABLED: true,
    DISABLED: false,
  },
};

const CATEGORY = {
  PUBLIC_DATA: ["id", "vi", "en", "thumbnail", "parentId", "id"],
};

const XERO = {
  INVOICE: {
    TYPE: {
      ACCPAY: "ACCPAY",
      ACCPAYCREDIT: "ACCPAYCREDIT",
      APOVERPAYMENT: "APOVERPAYMENT",
      APPREPAYMENT: "APPREPAYMENT",
      ACCREC: "ACCREC",
      ACCRECCREDIT: "ACCRECCREDIT",
      AROVERPAYMENT: "AROVERPAYMENT",
      ARPREPAYMENT: "ARPREPAYMENT",
    },
    STATUS: {
      DRAFT: "DRAFT",
      SUBMITTED: "SUBMITTED",
      DELETED: "DELETED",
      AUTHORISED: "AUTHORISED",
      PAID: "PAID",
      VOIDED: "VOIDED",
    },
  },
  LINE_ACCOUNT_TYPE: {
    EXCLUSIVE: "Exclusive",
    INCLUSIVE: "Inclusive",
    NOTAX: "NoTax",
  },
  DATE_FORMAT: "Y-MM-DD",
  CLIENT_ID: config.XERO_CLIENT_ID,
  SECRET: config.XERO_SECRET,
  TENANT_ID: config.XERO_TENANT_ID,
  REV_USER_RECEIVABLE: config.XERO_REV_USER_RECEIVABLE,
  COST_MERCHANT: config.XERO_COST_MERCHANT,
  REV_ADMIN_FEES: config.XERO_REV_ADMIN_FEES,
  REV_MERCHANT_FEES: config.XERO_REV_MERCHANT_FEES,
  COST_PG: config.XERO_COST_PG,
  BANK1: config.XERO_BANK1,
};

const EPAY = {
  MERCHANT_ID: config.EPAY_MERCHANT_ID,
  ENCODE_KEY: config.EPAY_ENCODE_KEY,
  CANCEL_PASSWORD: config.EPAY_CANCEL_PASSWORD,
  DOMAIN: config.EPAY_BASE_URL,
  KEY3DES_ENCRYPT: config.EPAY_KEY3DES_ENCRYPT,
  KEY3DES_DECRYPT: config.EPAY_KEY3DES_DECRYPT,
  PAY_OPTIONS: {
    PAY_AND_CREATE_TOKEN: "PAY_AND_CREATE_TOKEN",
    PAY_WITH_TOKEN_API: "PAY_WITH_TOKEN_API",
    PAY_WITH_RETURNED_TOKEN: "PAY_WITH_RETURNED_TOKEN",
    PURCHASE_OTP: "PURCHASE_OTP",
  },
  PAY_TYPE: {
    IC: "IC",
    DC: "DC",
    EW: "EW",
    VA: "VA",
    NO: "NO",
  },
  WINDOW_TYPE_MAP: {
    [WINDOW_TYPE.DESKTOP]: "0",
    [WINDOW_TYPE.MOBILE]: "1",
  },
  RESPONSE_CODE: require("./epay-response-code"),
  STATUS: {
    FAILED: "-3", // Original transaction fails
    PENDING: "-2", // Original transaction is pending
    NOT_FOUND: "-1", // Transaction not found
    SUCCESS_NO_REFUND: "0", // Original transaction is successful (no refund)
    SUCCESS_REFUND_MADE: "2", // Original transaction is successful (refund already made)
  },
  NOTIFY_URL_BY_TYPE: {
    [PAYMENT_GATEWAY_REQUEST.TYPE.CHARGE_QR_3D_SECURE]: "/weho/ep/3d/QrPr",
    [PAYMENT_GATEWAY_REQUEST.TYPE.CHARGE_AIO_3D_SECURE]: "/weho/ep/3d/AioPr",
  },
  MAP_CARD_TYPE: {
    IC: PAYMENT_METHOD.TYPE.CREDIT_CARD,
    DC: PAYMENT_METHOD.TYPE.ATM,
    EW: PAYMENT_METHOD.TYPE.E_WALLET,
    VA: PAYMENT_METHOD.TYPE.VIRTUAL_ACCOUNT,
    NO: PAYMENT_METHOD.TYPE.NO,
  },
  MAP_CARD_TYPE_TO_PAY_TYPE: {
    [PAYMENT_METHOD.TYPE.CREDIT_CARD]: "IC",
    [PAYMENT_METHOD.TYPE.ATM]: "DC",
    [PAYMENT_METHOD.TYPE.E_WALLET]: "EW",
    [PAYMENT_METHOD.TYPE.VIRTUAL_ACCOUNT]: "VA",
    [PAYMENT_METHOD.TYPE.NO]: "NO",
  },
  ATM_PERCENTAGE: parseFloat(config.EPAY_ATM_PERCENTAGE),
  ATM_AMOUNT: parseFloat(config.EPAY_ATM_AMOUNT),
  CREDIT_AMOUNT: parseFloat(config.EPAY_CREDIT_AMOUNT),
  CREDIT_PERCENTAGE: parseFloat(config.EPAY_CREDIT_PERCENTAGE),
};

const MAP_CARD_TYPE_TO_PAY_OPTION = {
  [PAYMENT_METHOD.TYPE.CREDIT_CARD]: EPAY.PAY_OPTIONS.PAY_AND_CREATE_TOKEN,
  [PAYMENT_METHOD.TYPE.ATM]: EPAY.PAY_OPTIONS.PAY_WITH_RETURNED_TOKEN,
};

const PURCHASE_ACCOUNTING = {
  LABEL: {
    PURCHASE_RECEIVABLE_INVOICE_ID: "PURCHASE_RECEIVABLE_INVOICE_ID",
    ADMIN_FEE_RECEIVABLE_INVOICE_ID: "ADMIN_FEE_RECEIVABLE_INVOICE_ID",
    ADMIN_FEE_PAYMENT_ID: "ADMIN_FEE_PAYMENT_ID",
    INSTALLMENT_PAYMENT_ID: "INSTALLMENT_PAYMENT_ID",
    PAYMENT_GATEWAY_PAYABLE_INVOICE_ID: "PAYMENT_GATEWAY_PAYABLE_INVOICE_ID",
    MERCHANT_PAYABLE_INVOICE_ID: "MERCHANT_PAYABLE_INVOICE_ID",
    MERCHANT_COMMISSION_RECEIVABLE_INVOICE_ID:
      "MERCHANT_COMMISSION_RECEIVABLE_INVOICE_ID",
  },
};

const KALAPA = {
  UNIQUE_PHOTO_MAX_QTY: 3,
  MINIMUM_ANTIFRAUD_SCORE: 700,
  MINIMUM_SALARY_LEVEL: 15,
  DOCUMENT_TYPE: {
    CCCD: "CCCD",
    CMDB: "CMDB",
  },
  GENDER_MAPPING: {
    Nam: USER.GENDER.MALE,
    Nu: USER.GENDER.FEMALE,
  },
  API_KEY: config.KALAPA_KEY,
};

const STORAGE = {
  KYC: config.S3_KYC_BUCKET,
};

const GOOGLE = {
  CLIENT_ID: config.GOOGLE_CLIENT_ID,
  SECRET: config.GOOGLE_CLIENT_SECRET,
};

const SMS = {
  PROVIDER: config.SMS_PRODIVDER,
};

const QUEUE = {
  CONFIRM_PURCHASE: config.CONFIRM_PURCHASE_QUEUE,
  UPDATE_DB: config.UPDATE_DB_QUEUE,
};

const MYSQL = {
  DATABASE: config.MYSQL_DATABASE,
  USER: config.MYSQL_USER,
  PASSWORD: config.MYSQL_PASSWORD,
  HOST: config.MYSQL_HOST,
  PORT: config.MYSQL_PORT,
};

const AWS = {
  ACCESS_KEY: config.AWS_ACCESS_KEY,
  ACCESS_SECRET: config.AWS_ACCESS_SECRET,
  S3_DEFAULT_BUCKET: config.S3_DEFAULT_BUCKET,
  REGION: config.AWS_REGION,
  SQS_DEFAULT_QUEUE: config.SQS_DEFAULT_QUEUE,
};

const ONESIGNAL = {
  USER_APP_ID: config.ONESIGNAL_USER_APP_ID,
  USER_API_KEY: config.ONESIGNAL_USER_API_KEY,
  POS_APP_ID: config.ONESIGNAL_POS_APP_ID,
  POS_API_KEY: config.ONESIGNAL_POS_API_KEY,
};

const SOUTH_TELECOM = {
  PREFIX: config.SOUTH_TELECOM_PREFIX,
  AUTHORIZATION: config.SMS_ST_AUTHORIZATION,
  BRAND_NAME: config.SMS_ST_BRANDNAME,
};

const VIETTEL = {
  USER: config.SMS_USER,
  PASSWORD: config.SMS_PASSWORD,
  CPCODE: config.SMS_CPCODE,
  BRAND_NAME: config.SMS_BRANDNAME,
};

const INCOM = {
  INCOM_SMS_URL: config.INCOM_SMS_URL,
  USERNAME: config.INCOM_USERNAME,
  PASSWORD: config.INCOM_PASSWORD,
  PREFIXED: config.INCOM_PREFIXED,
  COMMANDCODE: config.COMMANDCODE,
};

const NOTIFICATION_CHANNELS = {
  PUSH_NOTIFICATION: "push_notification",
};

const EMAIL = {
  PREFIX: config.EMAIL_PREFIX,
  SENDER_NAME: config.EMAIL_SENDER_NAME,
  SENDER_ADDRESS: config.EMAIL_SENDER_ADDRESS,
};

const BLOCK_PAYMENT_FEATURE = {
  BLOCK_NAPAS_PAYMENT: config.BLOCK_NAPAS_PAYMENT === "true",
  BLOCK_CREDIT_PAYMENT: config.BLOCK_CREDIT_PAYMENT === "true",
  BLOCK_ADD_NAPAS: config.BLOCK_ADD_NAPAS === "true",
  BLOCK_ADD_CREDIT: config.BLOCK_ADD_CREDIT === "true",
};

module.exports = {
  RUN_CRON: config.RUN_CRON,
  BLOCK_PAYMENT_FEATURE,
  GOOGLE,
  EMAIL,
  VIETTEL,
  INCOM,
  NOTIFICATION_CHANNELS,
  SOUTH_TELECOM,
  ONESIGNAL,
  SMS,
  AWS,
  MYSQL,
  QUEUE,
  DEFAULT_MERCHANT_COMMISION,
  STORAGE,
  KALAPA,
  PURCHASE_ACCOUNTING,
  CATEGORY,
  MARKETING_CATEGORY,
  XERO,
  SMS_LOGS,
  CONFIGURATION,
  MOBILE_POS_PURCHASE,
  KYC,
  AUTH_TOKEN_EXPIRATION,
  AUTH_TOKEN_SECRET,
  EMAIL_TOKEN_EXPIRATION,
  EMAIL_TOKEN_SECRET,
  BASE_URL,
  DEFAULT_COUNTRY,
  PASSWORD_TOKEN_EXPIRATION,
  PASSWORD_TOKEN_SECRET,
  FRONT_END_BASE_URL,
  CREDIT_HISTORY_TYPE,
  AUTH_LIT_USER_TOKEN_SECRET,
  AUTH_LIT_USER_TOKEN_EXPIRATION,
  DEFAULT_PAGE_LENGTH: 10,
  DEFAULT_CURRENCY: "vnd",
  DEFAULT_LOCALE: "vn-VN",
  USER_DATA_VIEW_BY_ADMIN,
  ROLES,
  PURCHASE_PAYMENT_STATUS,
  INSTALLMENT,
  MERCHANT,
  MERCHANT_PUBLIC_DATA,
  CONFIRM_PURCHASE_QUEUE,
  UPDATE_DB_QUEUE,
  QUERY_METHOD_MAPPING,
  PURCHASE,
  RETRY,
  TRANSACTION_FEE,
  PINCODE,
  PURCHASE_SEARCH_TYPE_TO_STATUS,
  AUTH,
  PAYMENT_GATEWAY_REQUEST,
  MERCHANT_STAFF,
  DEVOPS_EMAILS,
  YOPAYMENT,
  DEFAULT_DATETIME_FORMAT: "Y-MM-DD H:m:s",
  DEFAULT_DATE_FORMAT: "Y-MM-DD",
  errorTags: require("./error-tags"),
  MERCHANT_TYPE_HAVE_ONLINE,
  INIT_TOKEN_FEE: config.INIT_TOKEN_FEE,
  WINDOW_TYPE,
  EPAY,
  MSG: require("./messages"),
  ADDRESS,
  CITY,
  DISTRICT,
  WARD,
  USER,
  COMPANY,
  MERCHANT_LIST,
  POS_DEVICE,
  ADMIN_FEE,
  ADMIN_FE_BASE_URL,
  MERCHANT_FE_BASE_URL,
  MERCHANT_FINANCIAL,
  PAYMENT_METHOD,
  MAP_CARD_TYPE_TO_PAY_OPTION,
  LATE_FEE: require("./late-fee-v2"),
  PURCHASE_CONFIG: require("./purchase"),
  PROMOTION: require("./promotion"),
  MARKETING,
  LIMIT_BY_TIER,
};

module.exports = {
  type: 'object',
  required: [
    'merId',
    'merTrxId',
    'amount',
    'goodsAmount',
    'userFee',
    'currency',
    'payType',
    'timeStamp',
    'invoiceNo',
    'goodsNm',
    'notiUrl',
    'merchantToken',
    'payOption',
    'payToken',
    'userId',
  ],
  properties: {
    merId: {
      type: 'string',
      maxLength: 10,
    },
    merTrxId: {
      type: 'string',
      maxLength: 50,
    },
    amount: {
      type: 'string',
      maxLength: 12,
    },
    goodsAmount: {
      type: 'string',
      maxLength: 12,
    },
    userFee: {
      type: 'string',
      maxLength: 12,
    },
    currency: {
      type: 'string',
      maxLength: 3,
      minLength: 3,
    },
    payType: {
      type: 'string',
      maxLength: 2,
    },
    timeStamp: {
      type: 'string',
      maxLength: 13,
    },
    fee: {
      type: 'string',
      maxLength: 12,
    },
    vat: {
      type: 'string',
      maxLength: 12,
    },
    notax: {
      type: 'string',
      maxLength: 12,
    },
    invoiceNo: {
      type: 'string',
      maxLength: 40,
    },
    goodsNm: {
      type: 'string',
      maxLength: 200,
    },
    notiUrl: {
      type: 'string',
      maxLength: 255,
    },
    merchantToken: {
      type: 'string',
      maxLength: 255,
    },
    payOption: {
      type: 'string',
      maxLength: 100,
    },
    payToken: {
      type: 'string',
      maxLength: 100,
    },
    userId: {
      type: 'string',
      maxLength: 40,
    },
  },
};

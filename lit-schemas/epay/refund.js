module.exports = {
  type: 'object',
  required: ['trxId', 'merId', 'merTrxId', 'amount', 'payType', 'timeStamp', 'merchantToken', 'cancelPw'],
  properties: {
    trxId: {
      type: 'string',
      maxLength: 40,
    },
    merId: {
      type: 'string',
      maxLength: 10,
    },
    merTrxId: {
      type: 'string',
      maxLength: 50,
    },
    amount: {
      type: 'string',
      maxLength: 12,
    },
    payType: {
      type: 'string',
      maxLength: 2,
    },
    timeStamp: {
      type: 'string',
      maxLength: 13,
    },
    merchantToken: {
      type: 'string',
      maxLength: 255,
    },
    cancelPw: {
      type: 'string',
      maxLength: 255,
    },
  },
};
